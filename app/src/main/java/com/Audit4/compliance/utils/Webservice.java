package com.Audit4.compliance.utils;

import android.util.Log;

import com.Audit4.compliance.GLOBAL_SETTINGS;
import com.Audit4.compliance.WebsiteUrl;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by AVDEVS_Workstation on 08-12-2016.
 */

public class Webservice {

    /**
     * Common method to make HTTP Connection call
     **/
    private static String makeHttpConnection(URL url, JSONObject postDataParams) {
        String response = "";
        try {
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");// read the response
            conn.setConnectTimeout(30000);
            conn.setDoInput(true);
            conn.setDoOutput(true);

            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(getPostDataString(postDataParams));

            writer.flush();
            writer.close();
            os.close();

            int responseCode = conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {

                BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuffer sb = new StringBuffer("");
                String line = "";

                while ((line = in.readLine()) != null) {

                    sb.append(line);
                    break;
                }

                in.close();
                conn.disconnect();
                return sb.toString();

            } else {
                conn.disconnect();
                return new String("false : " + responseCode);
            }

        } catch (SocketTimeoutException e) {
            Log.i("makeHttpConnection", "Timeout exception occurred");

            e.printStackTrace();
            return Util.TIME_OUT_EXCEPTION;
        }catch (UnknownHostException e){
            Log.i("makeHttpConnection", "UnknownHostException occurred");
            e.printStackTrace();
            response = Util.UNKOWN_HOST_EXCEPTION;
        }catch (ConnectException e){
            Log.i("makeHttpConnection", "ConnectException occurred");
            response = Util.CONNECTION_OUT_EXCEPTION;
        }catch (Exception e){
            Log.i("makeHttpConnection", "exception occurred");
            response = Util.EXCEPTION_OCCUR;
        }
        return response;
    }

    public static String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while (itr.hasNext()) {

            String key = itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }

    public static String getLogin(String username, String password) {

        String response = null;
        URL url;
        try {
            url = new URL(WebsiteUrl.webURl + "includes/APIS/login.php");

            JSONObject postDataParams = new JSONObject();


            postDataParams.put("username", username);
            postDataParams.put("password", password);

            Log.e("params", postDataParams.toString());

            response = makeHttpConnection(url, postDataParams);

        } catch (Exception e) {
            Log.i("Error : ", "Exception Occurs getLogin");
        }
        return response;
    }

    public static String licenceCheck(String deviceID, String companyID) {

        String response = null;
        URL url;
        try {
            url = new URL(WebsiteUrl.webURl + "includes/APIS/checkLicence.php");
            Log.e("licenceCheck", WebsiteUrl.webURl + "includes/APIS/checkLicence.php");
            JSONObject postDataParams = new JSONObject();


            postDataParams.put("deviceID", deviceID);
            postDataParams.put("companyID", companyID);

            Log.e("params", postDataParams.toString());

            response = makeHttpConnection(url, postDataParams);

        } catch (Exception e) {
            Log.i("Error : ", "Exception Occurs getLogin");
        }
        return response;
    }

    public static String uploadAuditsToWebsite(String deviceID, String companyID) {

        String response = null;
        URL url;
        try {
            url = new URL(WebsiteUrl.webURl + "includes/APIS/checkLicence.php");

            JSONObject postDataParams = new JSONObject();


            postDataParams.put("deviceID", deviceID);
            postDataParams.put("companyID", companyID);

            Log.e("params", postDataParams.toString());

            response = makeHttpConnection(url, postDataParams);

        } catch (Exception e) {
            Log.i("Error : ", "Exception Occurs getLogin");
        }
        return response;
    }

    public static String obtainLicence(String deviceID, String companyID, String deviceName) {
        String response = null;
        URL url;

        try {
            url = new URL(WebsiteUrl.webURl + "includes/APIS/obtainLicence.php");
            Log.e("obtainLicense", WebsiteUrl.webURl + "includes/APIS/obtainLicence.php");
            JSONObject postDataParams = new JSONObject();


            postDataParams.put("deviceID", deviceID);
            postDataParams.put("companyID", companyID);
            postDataParams.put("deviceName", deviceName);


            Log.e("params", postDataParams.toString());

            response = makeHttpConnection(url, postDataParams);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return response;
    }

    public static String uploadCompletedAudit(String[] params, boolean DEBUG) {
        String response = null;
        URL url;
        try {
            url = new URL(WebsiteUrl.webURl + "includes/APIS/uploadCompletedAudit.php");
            Log.e("uploadCompletedAudit", WebsiteUrl.webURl + "includes/APIS/uploadCompletedAudit.php");
            JSONObject postDataParams = new JSONObject();
            postDataParams.put("AuditDate", params[0]);
            postDataParams.put("TotalScore", params[1]);
            postDataParams.put("UserID", params[2]);
            postDataParams.put("AuditAssetID", params[3]);
            postDataParams.put("RouteName", params[4]);
            postDataParams.put("Client", params[6]);
            postDataParams.put("ClientSite", params[7]);
            postDataParams.put("CompanyID", params[8]);
            postDataParams.put("CompanyAuditID", params[9]);
            postDataParams.put("ServerID", params[10]);
            postDataParams.put("Team", params[11]);

            response = makeHttpConnection(url, postDataParams);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return response;
    }


    //params
    //0 = id
    //1 = result from server
    //2 = TemplateAuditAnswerID
    //3 = CompanyAuditID
    //4 = Additional Notes
    public static String uploadAuditAnswers(boolean DEBUG, String... params) {
        String response = null;
        URL url = null;


        try {


            JSONObject postDataParams = new JSONObject();
            postDataParams.put("CompletedAuditID", params[1]);
            postDataParams.put("TemplateAuditAnswerID", params[2] == null?"": params[2]);
            postDataParams.put("CompanyAuditAnswerID", params[3] == null ? "" : params[3]);
            postDataParams.put("AdditionalNotes", params[4]);
            Log.e("uploadAuditAnswers", "uploadAuditAnswers: "+params[4] );
            postDataParams.put("QuestionNumber", params[5]
            );

            if (params[2] == null && params[3] == null) {
                if (params[4].length() > 0) {
                    url = new URL(WebsiteUrl.webURl + "includes/APIS/uploadCompletedAudit.php");
                    Log.e("uploadAuditAnswers", url.toString());
                } else {
                    if (GLOBAL_SETTINGS.DEBUG || DEBUG) Log.d("Upload", "No Data to Upload");
                }
            } else {
                url = new URL(WebsiteUrl.webURl + "includes/APIS/uploadCompletedAuditAnswers.php");
            }

            response = makeHttpConnection(url, postDataParams);
        } catch (Exception e) {
            if (GLOBAL_SETTINGS.DEBUG || DEBUG)
                Log.e("log_tag", "Error on Upload: " + e.toString());
        }
        return response;
    }

    public static String Populate(String urlString, String key, String value) {
        String response = null;
        URL url;
        try {
            url = new URL(urlString);
            JSONObject postDataParams = new JSONObject();
            postDataParams.put(key, value);
            response = makeHttpConnection(url, postDataParams);

        } catch (Exception e) {

            Log.e("log_tag", "Error on Upload: " + e.toString());
        }
        return response;
    }

    public static String getAssetHeaders() {

        String response = null;
        URL url;
        try {
            url = new URL(WebsiteUrl.webURl + "includes/APIS/getAuditAssetHeaders.php");

            JSONObject postDataParams = new JSONObject();


            postDataParams.put("auditAssetTypeID", "all");

            Log.e("params", postDataParams.toString());

            response = makeHttpConnection(url, postDataParams);

        } catch (Exception e) {
            Log.i("Error : ", "Exception Occurs getLogin");
        }
        return response;
    }
}
