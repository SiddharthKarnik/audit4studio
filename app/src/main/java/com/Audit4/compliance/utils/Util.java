package com.Audit4.compliance.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

import com.Audit4.compliance.R;
import com.Audit4.compliance.login.Login;

import java.util.ArrayList;
import java.util.List;


public class Util {
    public static final int REQ_CODE_LOC_CAM = 1;
    public static final int REQUEST_PERMISSION_SETTING = 2;
    public static String UNKOWN_HOST_EXCEPTION = "false: unknown_host";
    public static String TIME_OUT_EXCEPTION = "false: time_out";
    public static String CONNECTION_OUT_EXCEPTION = "false: connection_out";
    public static String EXCEPTION_OCCUR = "false: exception_occured";

    public static boolean checkPermissions(String[] PERMISSIONS_ARRAY, int reqCode, Activity activity) {
        int perm;

        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String permission : PERMISSIONS_ARRAY) {
            perm = ContextCompat.checkSelfPermission(activity,
                    permission);
            if (perm != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(permission);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(activity,
                    listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), reqCode);
            return false;
        }

        return true;
    }

    public static boolean isInternetConnected(Context context) {

        ConnectivityManager conMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        boolean ret = true;
        if (conMgr != null) {
            NetworkInfo i = conMgr.getActiveNetworkInfo();

            if (i != null) {
                if (!i.isConnected()) {
                    ret = false;
                }

                if (!i.isAvailable()) {
                    ret = false;
                }
            }

            if (i == null)
                ret = false;
        } else
            ret = false;
        return ret;
    }

    public static boolean isString(String str) {

        return str != null && !str.isEmpty();
    }

    public static void showNoInternetAlert(final AppCompatActivity mActivity) {
        final AlertDialog.Builder alert = new AlertDialog.Builder(mActivity);

        alert.setTitle("Warning.");
        alert.setMessage("You don't have an active data connection, please check your connection and try again!");


        alert.setPositiveButton("Open Network Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                Intent intent = new Intent(Settings.ACTION_SETTINGS);
                //    intent.setClassName("com.android.phone", );
                mActivity.startActivity(intent);
                dialog.dismiss();
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });

        AlertDialog alertDialog = alert.show();
        Button positive = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
        positive.setTextColor(Color.parseColor("#027C71"));
        Button negative = alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE);
        negative.setTextColor(Color.parseColor("#027C71"));
    }

    public static void showCustomeAlertwithSingleButton(final AppCompatActivity mActivity, final String title, final String error) {
        final AlertDialog.Builder alert = new AlertDialog.Builder(mActivity);

        alert.setTitle(title);
        alert.setMessage(error);
        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = alert.show();
        Button positive = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
        positive.setTextColor(Color.parseColor("#027C71"));

    }

    public static void showErrorInResponse(AppCompatActivity mActivity, String result) {
        String error = mActivity.getResources().getString(R.string.exception_occured);
        if (result.equals(Util.TIME_OUT_EXCEPTION)) {
            error = mActivity.getResources().getString(R.string.exception_time_out);
        } else if (result.equals(Util.UNKOWN_HOST_EXCEPTION)) {
            error = mActivity.getResources().getString(R.string.exception_unkown_host);
        } else if (result.equals(Util.CONNECTION_OUT_EXCEPTION)) {
            error = mActivity.getResources().getString(R.string.exception_connection_out);
        }

//        Util.showCustomeAlertwithSingleButton((Login) mActivity, "Login Failed", error);
        Util.showCustomeAlertwithSingleButton( mActivity, "Login Failed", error);
    }
}
