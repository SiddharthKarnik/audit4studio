package com.Audit4.compliance;

import java.io.Serializable;

public class CompletedQuestion implements Serializable {
	private int questionNumber, questionRank, TemplateAuditQuestionID, CompanyAuditID;
	private String answerGrade, additionalNotes;
	private String[] imagePaths;
	
	public CompletedQuestion(int questionNumber, int questionRank, String answerGrade, String additionalNotes, String[] imagePaths, int TemplateAuditQuestionID, int CompanyAuditID){
		this.questionNumber = questionNumber;
		this.questionRank = questionRank;
		this.answerGrade = answerGrade;
		this.additionalNotes = additionalNotes;
		this.imagePaths = imagePaths;
		this.TemplateAuditQuestionID = TemplateAuditQuestionID;
		this.CompanyAuditID = CompanyAuditID;
	}
	
	public void setQuestionNumber(int questionNumber){
		this.questionNumber = questionNumber;
	}
	public int getQuestionNumber(){
		return questionNumber;
	}
	
	public void setAnswerGrade(String answerGrade){
		this.answerGrade = answerGrade;
	}
	public String getAnswerGrade(){
		return answerGrade;
	}
	
	public void setAdditionalNotes(String additionalNotes){
		this.additionalNotes = additionalNotes;
	}
	public String getAdditionalNotes(){
		return additionalNotes;
	}
	
	public void setImagePaths(String[] imagePaths) {
		this.imagePaths = imagePaths;
	}
	public String[] getImagePaths() {
		return imagePaths;
	}
	
	public int getQuestionRank()
	{
		return questionRank;
	}
	
	public int getTemplateAuditQuestionID()
	{
		return TemplateAuditQuestionID;
	}
	
	public int getCompanyAuditID()
	{
		return CompanyAuditID;
	}
}
