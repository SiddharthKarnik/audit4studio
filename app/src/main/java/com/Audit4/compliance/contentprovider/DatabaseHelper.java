package com.Audit4.compliance.contentprovider;

import java.io.IOException;

import com.Audit4.compliance.GLOBAL_SETTINGS;
import com.Audit4.compliance.login.FillAssetHeadersAsync;
import com.Audit4.compliance.login.LoginAsync;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHelper extends SQLiteOpenHelper {
	private static final String TAG = "DatabaseHelper"; //for log message use
	private static final String SQL_DIR = "sql"; //directory in the assets folder
    private static final String CREATEFILE = "vending.sql"; //sql filename
    private static final String UPGRADEFILE_PREFIX = "upgrade-";
    private static final String UPGRADEFILE_SUFFIX = ".sql";
    private Context context;
    
    private boolean DEBUG = false;
    
    public DatabaseHelper(Context context, String name, CursorFactory factory, int version) {
        super(context, name, factory, version);
        this.context = context ;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
        	if(GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d(TAG, "onCreate Create Database");
            execSqlFile(CREATEFILE, db);
        } 
        catch(IOException exception) {
        	if(GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d(TAG, "onCreate Create Database Failed");
        	throw new RuntimeException("Database creation failed", exception);
        }
    }

    @Override
	public void onOpen(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		super.onOpen(db);
	}

	@Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
        	if(GLOBAL_SETTINGS.DEBUG || this.DEBUG)  Log.d(TAG, "upgrade database from {} to {}" + oldVersion + newVersion);
            for(String sqlFile : AssetUtils.list(SQL_DIR, this.context.getAssets())) {
                if (sqlFile.startsWith(UPGRADEFILE_PREFIX)) {
                	int fileVersion = Integer.parseInt(sqlFile.substring(UPGRADEFILE_PREFIX.length(),  sqlFile.length() - UPGRADEFILE_SUFFIX.length())); 
                    if (fileVersion > oldVersion && fileVersion <= newVersion) {
                        execSqlFile(sqlFile, db);
                    }
                }
            }
        } 
        catch( IOException exception ) {
            throw new RuntimeException("Database upgrade failed", exception );
        }
    }
    
    /* LOADS AN SQL FILE THAT CREATES THE DATABASE */
    protected void execSqlFile(String sqlFile, SQLiteDatabase db) throws SQLException, IOException {
    	if(GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d(TAG, "exec sql file: {}" + sqlFile);
        //uses SQL Parser to split SQL file into individual SQL statements
        for(String sqlInstruction : SqlParser.parseSqlFile( SQL_DIR + "/" + sqlFile, this.context.getAssets())) {
        	if(GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d(TAG, "sql: {}" + sqlInstruction);
            db.execSQL(sqlInstruction); //execute individual sql statement
        }



    }
    /* LOADS AN SQL FILE THAT CREATES THE DATABASE */
}