package com.Audit4.compliance.contentprovider;

import android.app.Activity;
import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.Audit4.compliance.AuditItem;
import com.Audit4.compliance.GLOBAL_SETTINGS;
import com.Audit4.compliance.ReUploadAuditsInfo;
import com.Audit4.compliance.UploadAuditsInfo;
import com.Audit4.compliance.audit.Audit;
import com.Audit4.compliance.audit.QuestionList;
import com.Audit4.compliance.templates.Template;
import com.Audit4.compliance.templates.TemplateFragment;
import com.Audit4.compliance.uploadAuditAnswerImages;
import com.Audit4.compliance.uploadAuditAnswers;

import java.util.ArrayList;

public class VendingContentProvider extends ContentProvider {

    private static final String TAG = "ContentProvider"; //for log message use

    public static String tableName = "";

    static final String PROVIDER = "com.Audit4.compliance.vending";
    static final String AUTHORITY = "content://" + PROVIDER + "/" + tableName;
    public static final Uri CONTENT_URI = Uri.parse(AUTHORITY);
    static final String SINGLE_RECORD_MIME_TYPE = "vnd.android.cursor.item/vnd.example.vendingapp.contentprovider";
    static final String MULTIPLE_RECORDS_MIME_TYPE = "vnd.android.cursor.dir/vnd.example.vendingapp.contentprovider";

    public static SQLiteDatabase database;
    private DatabaseHelper dbHelper;
    public static long rowID;

    private boolean DEBUG = false;

    @Override
    public boolean onCreate() {
        if (GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d(TAG, "onCreate VendingContentProvider");

		/* Required to pass to DatabaseHelper */
        Context context = getContext();
        dbHelper = null;
		/* Required to pass to DatabaseHelper */

        try { //create new instance of DatabaseHelper
            dbHelper = new DatabaseHelper(context, "vending", null, VersionUtils.getVersionCode(context));
        } catch (NameNotFoundException e) {
            if (GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d(TAG, "onCreate: No database found");
            e.printStackTrace();
        }

        database = dbHelper.getWritableDatabase(); //get the created database

        if (database == null) {
            if (GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d(TAG, "onCreate: No database active");
            return false;
        } else {
            if (GLOBAL_SETTINGS.DEBUG || this.DEBUG)
                Log.d(TAG, "onCreate: database active - " + database);
            return true;
        }
    }

    @Override
    public String getType(Uri uri) {
        String ret = getContext().getContentResolver().getType(CONTENT_URI);
        if (GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d(TAG, "getType returning: " + ret);
        return ret;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {

        rowID = database.insert(tableName, "", values);
        if (rowID > 0) {
            Uri _uri = ContentUris.withAppendedId(CONTENT_URI, rowID);
            if (GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d("rowID", "Row ID: " + rowID);
            // getContext().getContentResolver().notifyChange(_uri, null);
            return _uri;
        }
        throw new SQLException("Failed to add a record into " + uri);

        //return null;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        if (GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d(TAG, "update uri: " + uri.toString());
        return 0;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        if (GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d(TAG, "delete uri: " + uri.toString());
        return 0;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        if (GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d(TAG, "query with uri: " + uri.toString());
        return null;
    }

    public boolean recordExists(int _id, String tableName) throws SQLException {
        Cursor cursor = database.query(true, tableName, new String[]{"_id"}, "_id" + "=" + _id, null, null, null, null, null);
        if (GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d(TAG, "recordExists: Cursor = " + cursor);
        if (cursor.moveToFirst()) {
            cursor.close();
            return true;
        } else {
            cursor.close();
            return false;
        }
    }

    public void clearTables() {
        database.delete("QuestionRank", null, null);
        database.delete("QuestionType", null, null);
        database.delete("CompanyAudit", null, null);
        database.delete("CompanyAuditAnswer", null, null);
        database.delete("CompanyAuditQuestion", null, null);
        database.delete("TemplateAudit", null, null);
        database.delete("TemplateAuditAnswer", null, null);
        database.delete("TemplateAuditQuestion", null, null);
        database.delete("TemplateAuditQuestionSection", null, null);
        database.delete("AuditAssetHeaders", null, null);
    }

    //Question Rank ID
    public void insertQuestionRank(int QuestionRankID, String QuestionRank, int RedScore, int AmberScore, int GreenScore) {
        ContentValues dataToInsert = new ContentValues();

        dataToInsert.put("_id", QuestionRankID);
        dataToInsert.put("QuestionRank", QuestionRank);
        dataToInsert.put("RedScore", RedScore);
        dataToInsert.put("AmberScore", AmberScore);
        dataToInsert.put("GreenScore", GreenScore);

        tableName = "QuestionRank";
        if (!recordExists(QuestionRankID, "QuestionRank")) {
            insert(CONTENT_URI, dataToInsert);
        }
    }

    //Question Type
    public void insertQuestionType(int QuestionID, String QuestionType, int NumberOfAnswers) {
        ContentValues dataToInsert = new ContentValues();

        dataToInsert.put("_id", QuestionID);
        dataToInsert.put("QuestionType", QuestionType);
        dataToInsert.put("NumberOfAnswers", NumberOfAnswers);

        tableName = "QuestionType";
        if (!recordExists(QuestionID, "QuestionType")) {
            insert(CONTENT_URI, dataToInsert);
        }
    }

    //Insert CompanyAudit
    public void insertCompanyAudit(int CompanyAuditID, String CompanyAuditName, int CompanyAuditBenchmark, int TemplateAuditID) {
        ContentValues dataToInsert = new ContentValues();

        dataToInsert.put("_id", CompanyAuditID);
        dataToInsert.put("CompanyAuditName", CompanyAuditName);
        dataToInsert.put("CompanyAuditBenchmark", CompanyAuditBenchmark);
        dataToInsert.put("TemplateAuditID", TemplateAuditID);

        tableName = "CompanyAudit";

        if (!recordExists(CompanyAuditID, "CompanyAudit")) {
            insert(CONTENT_URI, dataToInsert);
        }
    }

    //Insert CompanyAuditAnswer
    public void insertCompanyAuditAnswer(int CompanyAuditAnswerID, String CompanyAuditAnswerGrade, String CompanyAuditAnswer, int CompanyAuditQuestionID) {
        ContentValues dataToInsert = new ContentValues();

        dataToInsert.put("_id", CompanyAuditAnswerID);
        dataToInsert.put("CompanyAuditAnswerGrade", CompanyAuditAnswerGrade);
        dataToInsert.put("CompanyAuditAnswer", CompanyAuditAnswer);
        dataToInsert.put("CompanyAuditQuestionID", CompanyAuditQuestionID);

        tableName = "CompanyAuditAnswer";

        if (!recordExists(CompanyAuditAnswerID, "CompanyAuditAnswer")) {
            insert(CONTENT_URI, dataToInsert);
        }

    }

    //Insert CompanyAuditQuestion
    public void insertCompanyAuditQuestion(int CompanyAuditQuestionID, String CompanyAuditQuestionTitle, String CompanyAuditQuestion, int QuestionTypeID, int QuestionRankID, int CompanyAuditID, int TemplateAuditQuestionSectionID) {
        ContentValues dataToInsert = new ContentValues();

        dataToInsert.put("_id", CompanyAuditQuestionID);
        dataToInsert.put("CompanyAuditQuestionTitle", CompanyAuditQuestionTitle);
        dataToInsert.put("CompanyAuditQuestion", CompanyAuditQuestion);
        dataToInsert.put("QuestionTypeID", QuestionTypeID);
        dataToInsert.put("QuestionRankID", QuestionRankID);
        dataToInsert.put("CompanyAuditID", CompanyAuditID);
        dataToInsert.put("TemplateAuditQuestionSectionID", TemplateAuditQuestionSectionID);

        tableName = "CompanyAuditQuestion";

        if (!recordExists(CompanyAuditQuestionID, "CompanyAuditQuestion")) {
            insert(CONTENT_URI, dataToInsert);
        }
    }

    //Insert TemplateAudit
    public void insertTemplateAudit(int TemplateAuditID, String TemplateAuditName, int AuditType) {
        ContentValues dataToInsert = new ContentValues();

        dataToInsert.put("_id", TemplateAuditID);
        dataToInsert.put("TemplateAuditName", TemplateAuditName);
        dataToInsert.put("AuditType", AuditType);

        tableName = "TemplateAudit";

        if (!recordExists(TemplateAuditID, "TemplateAudit")) {
            insert(CONTENT_URI, dataToInsert);
        }

    }

    //InsertTemplateAuditAnswer
    public void insertTemplateAuditAnswer(int TemplateAuditAnswerID, String TemplateAuditAnswerGrade, String TemplateAuditAnswer, int TemplateAuditQuestionID) {
        ContentValues dataToInsert = new ContentValues();

        dataToInsert.put("_id", TemplateAuditAnswerID);
        dataToInsert.put("TemplateAuditAnswerGrade", TemplateAuditAnswerGrade);
        dataToInsert.put("TemplateAuditAnswer", TemplateAuditAnswer);
        dataToInsert.put("TemplateAuditQuestionID", TemplateAuditQuestionID);

        tableName = "TemplateAuditAnswer";

        if (!recordExists(TemplateAuditAnswerID, "TemplateAuditAnswer")) {
            insert(CONTENT_URI, dataToInsert);
        }

    }

    //InsertTemplateAuditQuestion
    public void insertTemplateAuditQuestion(int TemplateAuditQuestionID, int TemplateAuditQuestionSectionID, String TemplateAuditQuestionTitle, String TemplateAuditQuestion, int QuestionTypeID, int QuestionRankID, int TemplateAuditID) {
        ContentValues dataToInsert = new ContentValues();

        dataToInsert.put("_id", TemplateAuditQuestionID);
        dataToInsert.put("TemplateAuditQuestionSectionID", TemplateAuditQuestionSectionID);
        dataToInsert.put("TemplateAuditQuestionTitle", TemplateAuditQuestionTitle);
        dataToInsert.put("TemplateAuditQuestion", TemplateAuditQuestion);
        dataToInsert.put("QuestionTypeID", QuestionTypeID);
        dataToInsert.put("QuestionRankID", QuestionRankID);
        dataToInsert.put("TemplateAuditID", TemplateAuditID);

        tableName = "TemplateAuditQuestion";

        if (!recordExists(TemplateAuditQuestionID, "TemplateAuditQuestion")) {
            insert(CONTENT_URI, dataToInsert);
        }

    }

    //InsertTemplateAuditQuestionSection
    public void insertTemplateAuditQuestionSection(int TemplateAuditQuestionSectionID, String TemplateAuditSectionName, int TemplateAuditID) {
        ContentValues dataToInsert = new ContentValues();

        dataToInsert.put("_id", TemplateAuditQuestionSectionID);
        dataToInsert.put("TemplateAuditSectionName", TemplateAuditSectionName);
        dataToInsert.put("TemplateAuditID", TemplateAuditID);

        tableName = "TemplateAuditQuestionSection";

        if (!recordExists(TemplateAuditQuestionSectionID, "TemplateAuditQuestionSection")) {
            insert(CONTENT_URI, dataToInsert);
        }
    }

    //Insert CompletedAudit
    public void insertCompleteAudit(int TotalScore, String AuditAssetID, String RouteID, String Date, String UserID, String Client, String ClientSite, String CompanyAuditID) {

        ContentValues dataToInsert = new ContentValues();

        dataToInsert.put("AuditDate", Date);
        dataToInsert.put("TotalScore", TotalScore);
        dataToInsert.put("UserID", UserID);
        dataToInsert.put("AuditAssetID", AuditAssetID);
        dataToInsert.put("RouteID", RouteID);
        dataToInsert.put("Client", Client);
        dataToInsert.put("ClientSite", ClientSite);
        dataToInsert.put("CompanyAuditID", CompanyAuditID);

        tableName = "CompletedAudit";

        insert(CONTENT_URI, dataToInsert);
    }

    //Get Last Completed Audit ID
    public int getLastAudit() {
        int id = -1;

        tableName = "CompletedAudit";

        String selectQuery = "SELECT * FROM " + tableName + " ORDER BY _id DESC limit 1";
        Cursor cursor = database.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            id = cursor.getInt(0);
        }
        cursor.close();
        return id;
    }

    //Insert CompletedAuditAnswers
    public void insertCompletedAuditAnswer(int CompletedAuditID, int TemplateAuditAnswerID, int CompanyAuditAnswerID, String AdditionalNotes, int QuestionNumber) {
        ContentValues dataToInsert = new ContentValues();

        dataToInsert.put("CompletedAuditID", CompletedAuditID);
        if (TemplateAuditAnswerID != -1) {
            dataToInsert.put("TemplateAuditAnswerID", TemplateAuditAnswerID);
        }
        if (CompanyAuditAnswerID != -1) {
            dataToInsert.put("CompanyAuditAnswerID", CompanyAuditAnswerID);
        }
        dataToInsert.put("AdditionalNotes", AdditionalNotes);

        dataToInsert.put("QuestionNumber", QuestionNumber);
        tableName = "CompletedAuditAnswer";
        insert(CONTENT_URI, dataToInsert);

    }

    //Insert CompletedAuditAnswerImage
    public void insertCompletedAuditAnswerImage(int CompletedAuditID, int TemplateAuditAnswerID, int CompanyAuditAnswerID, String[] ImagePaths) {
        ContentValues dataToInsert = new ContentValues();

        tableName = "CompletedAuditAnswerImage";

        for (int i = 0; i < ImagePaths.length; i++) {
            if (ImagePaths[i] != null) {
                if (GLOBAL_SETTINGS.DEBUG || this.DEBUG)
                    Log.d("Image Path", "Image: " + ImagePaths[i]);
                if (TemplateAuditAnswerID != -1) {
                    dataToInsert.put("TemplateAuditAnswerID", TemplateAuditAnswerID);
                }
                if (CompanyAuditAnswerID != -1) {
                    dataToInsert.put("CompanyAuditAnswerID", CompanyAuditAnswerID);
                }
                dataToInsert.put("CompletedAuditID", CompletedAuditID);
                dataToInsert.put("ImagePath", ImagePaths[i]);
                insert(CONTENT_URI, dataToInsert);
            }
        }
    }

    //GetTemplateList
    public void getCompanyTemplateList() {
        tableName = "CompanyAudit";

        String selectQuery = "SELECT * FROM " + tableName + " ORDER BY CompanyAuditName COLLATE NOCASE ASC";

        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                int companyAuditID = cursor.getInt(0);
                String companyAuditName = cursor.getString(1);
                int companyAuditBenchmark = cursor.getInt(2);
                int templateAuditID = cursor.getInt(3);

                Template template = new Template();
                template.setTemplate(companyAuditID, companyAuditName, companyAuditBenchmark, templateAuditID);
                TemplateFragment.listItems.add(template);
            } while (cursor.moveToNext());
        }
        cursor.close();
    }

    //Get the total number of sections in an audit
    public int getTotalSections(int templateID) {
        tableName = "TemplateAuditQuestionSection";
        String selectQuery = "SELECT * FROM " + tableName + " WHERE TemplateAuditID = " + templateID;

        Cursor cursor = database.rawQuery(selectQuery, null);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    //Get TemplateAuditQuestion
    public void getTemplateQuestions(int templateID, int templateSectionID) {
        tableName = "TemplateAuditQuestion";
        String selectQuery = "SELECT * FROM " + tableName + " WHERE TemplateAuditID = " + templateID + " AND TemplateAuditQuestionSectionID = " + templateSectionID;
        Cursor cursor = database.rawQuery(selectQuery, null);
        String[] answers = new String[3];

        while (cursor.moveToNext()) {
            String QuestionTitle = cursor.getString(1);
            String Question = cursor.getString(2);
            int Section = cursor.getInt(3);
            int QuestionRank = cursor.getInt(5);

            int TemplateAuditQuestionID = cursor.getInt(0);
            answers = getTemplateAnswers(TemplateAuditQuestionID);

            QuestionList QL = new QuestionList();
            QL.setQuestions(QuestionTitle, QuestionRank, Question, Section, answers[0], answers[1], answers[2], TemplateAuditQuestionID, -1);
            Audit.questionList.add(QL);
        }
        cursor.close();
    }

    //Get CompanyAuditQuestions
    public void getCompanyAuditQuestions(int templateID, int templateSectionID, String templateName) {
        tableName = "CompanyAudit";
        String selectQuery = "SELECT * FROM " + tableName + " WHERE TemplateAuditID = " + templateID + " AND CompanyAuditName = \"" + templateName + "\"";

        Cursor cursor = database.rawQuery(selectQuery, null);
        cursor.moveToFirst();
        int CompanyAuditID = cursor.getInt(0);
        String[] answers = new String[3];

        tableName = "CompanyAuditQuestion";
        selectQuery = "SELECT * FROM " + tableName + " WHERE CompanyAuditID = " + CompanyAuditID + " AND TemplateAuditQuestionSectionID = " + templateSectionID;

        cursor = database.rawQuery(selectQuery, null);

        while (cursor.moveToNext()) {
            String QuestionTitle = cursor.getString(1);
            String Question = cursor.getString(2);
            int Section = cursor.getInt(6);

            int CompanyAuditQuestionID = cursor.getInt(0);
            answers = getCompanyAnswers(CompanyAuditQuestionID);
            int QuestionRank = cursor.getInt(4);

            QuestionList QL = new QuestionList();
            QL.setQuestions(QuestionTitle, QuestionRank, Question, Section, answers[0], answers[1], answers[2], -1, CompanyAuditQuestionID);

            Audit.questionList.add(QL);
        }
        cursor.close();

    }

    //Get Section
    public String getSectionName(int SectionID) {
        tableName = "TemplateAuditQuestionSection";
        String selectQuery = "SELECT * FROM " + tableName + " WHERE _id = " + SectionID;

        Cursor cursor = database.rawQuery(selectQuery, null);
        cursor.moveToFirst();

        String str = cursor.getString(1);
        cursor.close();
        return str;
    }

    //Get Template Answers
    public String[] getTemplateAnswers(int TemplateAuditQuestionID) {
        String answers[] = new String[3];

        tableName = "TemplateAuditAnswer";
        String selectQuery = "SELECT * FROM " + tableName + " WHERE TemplateAuditQuestionID = " + TemplateAuditQuestionID;

        Cursor cursor = database.rawQuery(selectQuery, null);

        int i = 0;
        while (cursor.moveToNext()) {
            answers[i] = cursor.getString(2);
            i++;
        }
        cursor.close();
        return answers;
    }

    //Get Company Answers
    public String[] getCompanyAnswers(int CompanyAuditQuestionID) {
        String answers[] = new String[3];

        tableName = "CompanyAuditAnswer";
        String selectQuery = "SELECT * FROM " + tableName + " WHERE CompanyAuditQuestionID = " + CompanyAuditQuestionID;

        Cursor cursor = database.rawQuery(selectQuery, null);

        int i = 0;
        while (cursor.moveToNext()) {
            answers[i] = cursor.getString(2);
            i++;
        }
        cursor.close();
        return answers;
    }

    //Get Company Benchmark
    public int getCompanyBenchmark(String Template) {
        int benchmark = -1;

        tableName = "CompanyAudit";
        String selectQuery = "SELECT * FROM " + tableName + " WHERE CompanyAuditName = " + "\"" + Template + "\"";

        Cursor cursor = database.rawQuery(selectQuery, null);
        while (cursor.moveToNext()) {
            benchmark = cursor.getInt(2);
            if (GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d("Benchmark", "" + benchmark);
        }
        cursor.close();
        return benchmark;
    }

    //Get Score
    public int[] getScore(int questionRank, String answerGrade) {
        int score[] = new int[2];

        int adjustment = 0;

        switch (answerGrade) {
            case "L":
                adjustment = 0;
                break;
            case "M":
                adjustment = 1;
                break;
            case "H":
                adjustment = 2;
                break;
            case "NONE":
                adjustment = 0;
                break;
        }

        tableName = "QuestionRank";
        String selectQuery = "SELECT * FROM " + tableName + " WHERE _id = " + questionRank;

        Cursor cursor = database.rawQuery(selectQuery, null);

        if (GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d("VCP", "count" + cursor.getCount());
        while (cursor.moveToNext()) {
            if (answerGrade.equals("NONE")) {
                score[0] = 0;
            } //This question has no affect on percentage given
            else {
                score[0] = cursor.getInt(4);
            }//Total available
            if (GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d("VCP", "Score: " + cursor.getInt(4));
            score[1] = cursor.getInt(2 + adjustment); //Actual Score
        }
        cursor.close();
        return score;
    }

    //Get TemplateAnswerID
    public int getTemplateAnswerID(int TemplateAuditQuestionID, String TemplateAuditAnswerGrade) {
        int ID = -1;

        tableName = "TemplateAuditAnswer";
        String selectQuery = "SELECT * FROM " + tableName + " WHERE TemplateAuditQuestionID = " + TemplateAuditQuestionID + " AND TemplateAuditAnswerGrade = \"" + TemplateAuditAnswerGrade + "\"";

        Cursor cursor = database.rawQuery(selectQuery, null);
        while (cursor.moveToNext()) {
            ID = cursor.getInt(0);
        }
        cursor.close();
        return ID;
    }

    //Get CompanyAnswerID
    public int getCompanyAnswerID(int CompanyAuditQuestionID, String CompanyAuditAnswerGrade) {
        int ID = -1;

        tableName = "CompanyAuditAnswer";
        String selectQuery = "SELECT * FROM " + tableName + " WHERE CompanyAuditQuestionID = " + CompanyAuditQuestionID + " AND CompanyAuditAnswerGrade = \"" + CompanyAuditAnswerGrade + "\"";

        Cursor cursor = database.rawQuery(selectQuery, null);
        while (cursor.moveToNext()) {
            ID = cursor.getInt(0);
        }
        cursor.close();
        return ID;
    }

    //Get CompanyAuditID
    public String getCompanyAuditID(String template) {
        String selectQuery = "SELECT * FROM CompanyAudit WHERE CompanyAuditName = \"" + template + "\"";

        Cursor cursor = database.rawQuery(selectQuery, null);

        cursor.moveToFirst();
        String str = cursor.getString(0);
        cursor.close();
        return str;
    }

    //Get TemplatePosition
    public int getTemplatePosition(int AuditID, int pos) {
        String selectQuery = "SELECT * FROM TemplateAuditQuestionSection WHERE TemplateAuditID = \"" + AuditID + "\"";

        Cursor cursor = database.rawQuery(selectQuery, null);

        cursor.moveToPosition(pos - 1);

        int x = cursor.getInt(0);
        cursor.close();
        return x;
    }

    //GetTemplateAuditID
    public int getTemplateAuditID(int companyAuditID) {
        String selectQuery = "SELECT * FROM CompanyAudit WHERE _id = \"" + companyAuditID + "\"";

        Cursor cursor = database.rawQuery(selectQuery, null);

        cursor.moveToFirst();
        int x = cursor.getInt(3);
        cursor.close();
        return x;
    }

    //Get TemplateAuditType
    public int getTemplateType(int TemplateID) {
        Log.d("getTemplateType", " " + TemplateID);
        tableName = "TemplateAudit";
        String selectQuery = "SELECT * FROM " + tableName + " WHERE _id = " + TemplateID;

        Cursor cursor = database.rawQuery(selectQuery, null);
        cursor.moveToFirst();
        int x = cursor.getInt(2);
        cursor.close();
        return x;
    }


    //Get AuditsToUpload
    public boolean getAuditsToUpload() {
        //TODO SELECT ONLY THOSE WITH -1
        tableName = "CompletedAudit";
        String selectQuery = "SELECT * FROM " + tableName + " WHERE ServerID = -1";

        Cursor cursor = database.rawQuery(selectQuery, null);

        if (GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d("CompletedAudits", "" + cursor.getCount());

        if (cursor.getCount() > 0) {
            cursor.close();
            return true;
        } else {
            cursor.close();
            return false;
        }
    }

    public ArrayList<AuditItem> getCompletedAuditsForListFragment(Context context, Activity activity, ArrayList<AuditItem> items) {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String userID = prefs.getString("UserID", "null");


        if (GLOBAL_SETTINGS.DEBUG || this.DEBUG)
            Log.d("getCompleteAudits", "getCompletedAuditsForListFragment");

        String id = "";

        tableName = "CompletedAudit";


        String selectQuery = "SELECT * FROM " + tableName + " WHERE ServerID = -1 AND UserID = '" + userID + "' ORDER BY _id DESC";


        Cursor cursor = database.rawQuery(selectQuery, null);


        while (cursor.moveToNext())

        {

            //String[] params = {cursor.getString(4),cursor.getString(1),cursor.getString(3)};

            items.add(new AuditItem(cursor.getString(4), cursor.getString(1), cursor.getString(3), false, cursor.getString(9), cursor.getInt(0)));

        }
        cursor.close();
		
		/*selectQuery = "SELECT * FROM " + tableName + " WHERE ServerID != -1 AND UserID = '" + userID + "' ORDER BY _id DESC";

		
		cursor = database.rawQuery(selectQuery, null);
		
		
		while(cursor.moveToNext())
		
			{
						
			//String[] params = {cursor.getString(4),cursor.getString(1),cursor.getString(3)};
			
			items.add(new AuditItem(cursor.getString(4),cursor.getString(1),cursor.getString(3), true, cursor.getString(9), cursor.getInt(0)));
			
			}
		cursor.close();
		*/


        //THIS IS PROBABLY REDUNDANT NOW AS ONLY RELEVANT FOR 1 ITERATION OF AUDIT APP
        tableName = "UploadedAudits";


        selectQuery = "SELECT * FROM " + tableName + " WHERE ServerID = -2 AND UserID = '" + userID + "' ORDER BY _id DESC";


        cursor = database.rawQuery(selectQuery, null);
        while (cursor.moveToNext())

        {

            //String[] params = {cursor.getString(4),cursor.getString(1),cursor.getString(3)};

            items.add(new AuditItem(cursor.getString(3), cursor.getString(1), cursor.getString(2), true, cursor.getString(4), cursor.getInt(0)));

        }
        cursor.close();

        return items;
    }

    public ArrayList<AuditItem> getOldAuditsForListFragment(Context context, Activity activity, ArrayList<AuditItem> items) {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String userID = prefs.getString("UserID", "null");


        if (GLOBAL_SETTINGS.DEBUG || this.DEBUG)
            Log.d("getCompleteAudits", "getCompletedAuditsForListFragment");

        String id = "";

        tableName = "CompletedAudit";
        String selectQuery = "SELECT * FROM " + tableName + " WHERE ServerID != -1 AND UserID = '" + userID + "' ORDER BY _id DESC";

        Cursor cursor = database.rawQuery(selectQuery, null);


        cursor = database.rawQuery(selectQuery, null);


        while (cursor.moveToNext())

        {

            //String[] params = {cursor.getString(4),cursor.getString(1),cursor.getString(3)};

            items.add(new AuditItem(cursor.getString(4), cursor.getString(1), cursor.getString(3), true, cursor.getString(9), cursor.getInt(0)));

        }
        cursor.close();

        return items;
    }

    public String getCompletedAudits(Context context, AppCompatActivity activity) {
        if (GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d("getCompleteAudits", "Running");

        String id = "";
        if (getAuditsToUpload() == true) {

            tableName = "CompletedAudit";

            String selectQuery = "SELECT * FROM " + tableName + " WHERE ServerID = -1";

            Cursor cursor = database.rawQuery(selectQuery, null);

            //TODO only select audits which dont have a serverID

            if (cursor.moveToFirst()) {
                id = cursor.getString(0);

                UploadAuditsInfo newUpload = new UploadAuditsInfo(context, activity);

                String[] params = {cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(0), cursor.getString(6), cursor.getString(7), cursor.getString(8), cursor.getString(9)};
                newUpload.execute(params);
            }

            cursor.close();
        }

        if (GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d("ID", "id: " + id);
        return id;
    }

    public String reuploadAudit(Context context, AppCompatActivity activity, int ServerID) {
        String id = "";

        tableName = "CompletedAudit";

        String selectQuery = "SELECT * FROM " + tableName + " WHERE _id = " + ServerID;

        Cursor cursor = database.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            id = cursor.getString(0);

            ReUploadAuditsInfo newUpload = new ReUploadAuditsInfo(context, activity);

            String[] params = {cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(0), cursor.getString(6), cursor.getString(7), cursor.getString(8), cursor.getString(9)};
            newUpload.execute(params);
        }

        cursor.close();

        return id;
    }

    public void addToUploadedAudits(String Date, String UserID, String AssetID, String ServerID) {
        ContentValues dataToInsert = new ContentValues();

        dataToInsert.put("AuditDate", Date);
        dataToInsert.put("UserID", UserID);
        dataToInsert.put("AuditAssetID", AssetID);
        dataToInsert.put("ServerID", ServerID);

        tableName = "UploadedAudits";

        insert(CONTENT_URI, dataToInsert);
    }

    public void getCompletedAuditAnswers(String id, String result, AppCompatActivity activity) {

        tableName = "CompletedAuditAnswer";
        String selectQuery = "SELECT * FROM " + tableName + " WHERE CompletedAuditID = " + id;

        Cursor cursor = database.rawQuery(selectQuery, null);

        while (cursor.moveToNext()) {
            String[] params = new String[]{id, result, cursor.getString(4), cursor.getString(5), cursor.getString(6), cursor.getString(7)};
            uploadAuditAnswers uploadAnswers = new uploadAuditAnswers(activity);
            uploadAnswers.execute(params);
        }

        //TODO dont delete audits
        //String deleteQuery = "DELETE FROM CompletedAuditAnswer WHERE CompletedAuditID = " + id;
        cursor.close();
        //database.execSQL(deleteQuery);

    }

    public void getAuditImages(String id, String result, AppCompatActivity activity) {

        tableName = "CompletedAuditAnswerImage";
        String selectQuery = "SELECT * FROM " + tableName + " WHERE CompletedAuditID = " + id;

        Cursor cursor = database.rawQuery(selectQuery, null);

        if (GLOBAL_SETTINGS.DEBUG || this.DEBUG)
            Log.d("Image", "ID = " + id + ", Checking for Images - Found: " + cursor.getCount());


        while (cursor.moveToNext()) {
            if (GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d("IMAGE", cursor.getString(6));
            String[] params = {id, result, cursor.getString(4), cursor.getString(5), cursor.getString(6)};
            uploadAuditAnswerImages uploadImages = new uploadAuditAnswerImages(activity);
            uploadImages.execute(params);
        }

        //TODO dont delete audits
        //String deleteQuery = "DELETE FROM "+ tableName + " WHERE CompletedAuditID = " + id;
        cursor.close();
        //database.execSQL(deleteQuery);

    }

    public void UpdateServerID(String id, String result) {
        tableName = "CompletedAudit";
        String updateQuery = "UPDATE " + tableName + " SET ServerID = " + result + " WHERE _id = " + id;
        database.execSQL(updateQuery);

        //tableName = "UploadedAudits";
        //updateQuery = "UPDATE " + tableName + " SET ServerID = "+ result + " WHERE _id = " + id;
        //database.execSQL(updateQuery);
    }

    public void DeleteAllByUserID(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String userID = prefs.getString("UserID", "null");

        tableName = "CompletedAudit";
        String selectQuery = "SELECT * FROM " + tableName + " WHERE UserID = " + userID;

        Cursor cursor = database.rawQuery(selectQuery, null);

        while (cursor.moveToNext()) {
            String CompletedAuditID = cursor.getString(0);
            Log.d("ID: ", CompletedAuditID);
            DeleteCompletedAuditAnswer(CompletedAuditID);
            DeleteCompletedAuditImage(CompletedAuditID);
        }
        cursor.close();

        tableName = "CompletedAudit";
        String deleteQuery = "Delete FROM " + tableName + " WHERE UserID = " + userID;
        database.execSQL(deleteQuery);

        tableName = "UploadedAudits";
        deleteQuery = "Delete FROM " + tableName + " WHERE UserID = " + userID;
        database.execSQL(deleteQuery);
    }

    public void DeleteCompletedAuditAnswer(String CompletedAuditID) {
        tableName = "CompletedAuditAnswer";
        String deleteQuery = "DELETE FROM " + tableName + " WHERE CompletedAuditID = " + CompletedAuditID;

        database.execSQL(deleteQuery);
    }

    public void DeleteCompletedAuditImage(String CompletedAuditID) {
        tableName = "CompletedAuditAnswerImage";
        String deleteQuery = "DELETE FROM " + tableName + " WHERE CompletedAuditID = " + CompletedAuditID;

        database.execSQL(deleteQuery);
    }

    public void DeleteByCompletedAuditID(String CompletedAuditID) {
        DeleteCompletedAuditAnswer(CompletedAuditID);
        DeleteCompletedAuditImage(CompletedAuditID);

        tableName = "CompletedAudit";
        String deleteQuery = "Delete FROM " + tableName + " WHERE _id = " + CompletedAuditID;
        database.execSQL(deleteQuery);
    }

    public void DeleteOldCompletedAuditID(String string) {
        // TODO Auto-generated method stub
        tableName = "UploadedAudits ";
        String deleteQuery = "Delete FROM " + tableName + " WHERE _id = " + string;
        database.execSQL(deleteQuery);
    }

    public String[] getHeadersForAssetType(int assetType) {

        String[] headers = new String[5];

        tableName = "AuditAssetHeaders";
        String selectQuery = "SELECT * FROM " + tableName + " WHERE _id = " + assetType;

        Cursor cursor = database.rawQuery(selectQuery, null);

        if (cursor.getCount() == 0) {
            selectQuery = "SELECT * FROM " + tableName + " WHERE _id = 1";
            cursor = database.rawQuery(selectQuery, null);
        }

        cursor.moveToFirst();
        headers[0] = cursor.getString(1);
        headers[1] = cursor.getString(2);
        headers[2] = cursor.getString(3);
        headers[3] = cursor.getString(4);
        headers[4] = cursor.getString(5);

        Log.v("Database", "Header1=" + cursor.getString(1));

        Log.v("Database", "Header2=" + cursor.getString(2));
        Log.v("Database", "Header3=" + cursor.getString(3));
        Log.v("Database", "Header4=" + cursor.getString(4));
        Log.v("Database", "Header5=" + cursor.getString(5));
        cursor.close();

        //headers[0]=headers[1]=headers[2]=headers[3]=headers[4] = "";
        return headers;

    }

    public void addHeadersForAssetType(int assetType, String[] header) {

        tableName = "AuditAssetHeaders";

        ContentValues dataToInsert = new ContentValues();

        dataToInsert.put("_id", assetType);
        dataToInsert.put("Header1", header[0]);
        dataToInsert.put("Header2", header[1]);
        dataToInsert.put("Header3", header[2]);
        dataToInsert.put("Header4", header[3]);
        dataToInsert.put("Header5", header[4]);

        if (!recordExists(assetType, "AuditAssetHeaders")) {
            insert(CONTENT_URI, dataToInsert);
        }

    }


}
