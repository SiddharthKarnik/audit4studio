package com.Audit4.compliance.templates;

public class Template {

	private int companyAuditID;
	private String companyAuditName;
	private int companyAuditBenchmark;
	private int templateAuditID;
	
	public void setTemplate (int companyAuditID, String companyAuditName, int companyAuditBenchmark, int templateAuditID)
		{
		this.companyAuditID = companyAuditID;
		this.companyAuditName = companyAuditName;
		this.companyAuditBenchmark = companyAuditBenchmark;
		this.templateAuditID = templateAuditID;
		}
	
	public String getName(){
		return companyAuditName;
	}
	
	public int getcompanyAuditID(){
		return companyAuditID;
	}
	
	public int companyAuditBenchmark(){
		return companyAuditBenchmark;
	}
	
	public int templateAuditID(){
		return templateAuditID;
	}
}
