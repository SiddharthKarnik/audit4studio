package com.Audit4.compliance.templates;

import java.util.List;

import com.Audit4.compliance.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class TemplateAdapter extends ArrayAdapter<Template> {
	
	private List<Template> _list;
	private Context _context;
	private int _layout;
	
	public TemplateAdapter(Context context, int resource,
			List<Template> objects) {
		super(context, resource, objects);
		_context = context;
		_layout = resource;
		_list = objects;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View row = convertView;
		
		LayoutInflater inflater = (LayoutInflater)_context.getSystemService("layout_inflater") ;
		row = inflater.inflate(_layout, null);

		Template _template = _list.get(position);
		
		//Template Name
		TextView routetext = (TextView) row.findViewById(R.id.templateName);
		routetext.setText(_template.getName());
				
		return row;
	}
	

}
