package com.Audit4.compliance.templates;

import java.util.ArrayList;

import android.app.ActivityOptions;
import android.app.ListFragment;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.Audit4.compliance.StartAudit;
import com.Audit4.compliance.contentprovider.VendingContentProvider;
import com.Audit4.compliance.R;

public class TemplateFragment extends ListFragment{
	public static ArrayList<Template> listItems = new ArrayList<Template>();
	
	private VendingContentProvider vcp;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        listItems.clear();

        vcp = new VendingContentProvider();
        vcp.getCompanyTemplateList();
        setListAdapter(new TemplateAdapter(getActivity(), R.layout.template_row, listItems));



    }
	
	@Override
	public void onListItemClick(ListView l, View v, int pos, long id)
	{
		super.onListItemClick(l, v, pos, id);
		Intent intent = new Intent (getActivity(), StartAudit.class);
		intent.putExtra("Template", listItems.get(pos).getName());
		intent.putExtra("AuditID", listItems.get(pos).getcompanyAuditID());
		intent.putExtra("TemplateID", listItems.get(pos).templateAuditID());
		ActivityOptions opts=ActivityOptions.makeCustomAnimation(getActivity(),
				R.anim.fade_in, R.anim.fade_out);
		 startActivity(intent,opts.toBundle());
	}  

}