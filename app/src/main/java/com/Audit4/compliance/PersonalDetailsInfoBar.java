package com.Audit4.compliance;

import com.Audit4.compliance.contentprovider.VendingContentProvider;
import com.Audit4.compliance.login.Login;
import com.Audit4.compliance.R;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

public class PersonalDetailsInfoBar extends AppCompatActivity {
	
	private boolean DEBUG = false;
	protected VendingContentProvider vcp = new VendingContentProvider(); 
	
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        getMenuInflater().inflate(R.menu.navbar_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.Bt_Logout) 
    		{
        	vcp.clearTables();
        		LogoutTopButton();
        	}
    
    return super.onOptionsItemSelected(item);
    }
    
    public void LogoutTopButton()
    {
    	SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
		SharedPreferences.Editor editor = prefs.edit();
		editor.putBoolean("LoggedIn", false);
		editor.putString("Name", "");
		editor.putString("Email", "");
		editor.putString("CompanyID", "");
		editor.putString("CompanyName", "");
		editor.putString("UserID", "");
		editor.commit();
       	
    	Intent intent = new Intent(this, Login.class);
    	intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP| Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(intent);
    }
}
