package com.Audit4.compliance;

import java.util.List;


import android.app.DialogFragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class MyAuditRowAdapter extends ArrayAdapter<AuditItem> {

	private List<AuditItem> _list;
	private Context _context;
	private int _layout;

	public MyAuditRowAdapter(Context context, int resource,
			List<AuditItem> objects) {
		super(context, resource, objects);
		_context = context;
		_layout = resource;
		_list = objects;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View row = convertView;

		if (row == null) {
			LayoutInflater inflater = (LayoutInflater) _context.getSystemService("layout_inflater");
			row = inflater.inflate(_layout, null);
		}

		final AuditItem _item = _list.get(position);

		TextView title = (TextView) row.findViewById(R.id.auditassetid);
		title.setText("Audit Asset ID: " + _item.getTitle());
		TextView feed = (TextView) row.findViewById(R.id.datecompleted);
		feed.setText("Date: "+ _item.getDescription());
		
		TextView date = (TextView) row.findViewById(R.id.Date);
		
		SharedPreferences prefs =  PreferenceManager.getDefaultSharedPreferences(_context);
		
		date.setText("Auditor: " + prefs.getString("Name","Missing"));

		TextView uploaded = (TextView) row.findViewById(R.id.uploaded);
		
		if(_item.isUploaded()) uploaded.setText("on website");
		else uploaded.setText("to upload....");
		
		row.setOnClickListener(new OnClickListener() {

		    @Override
		    public void onClick(View v) {
		    	Log.d("ITem","" + _item.getServerID());
		    	if((int) Integer.parseInt(_item.getServerID())>0){ //audit already uploaded
		    		((CompletedAuditListFragment)_context).serverID =  _item.getDb_id(); //getDBIndex
		    		((CompletedAuditListFragment)_context).newDialog(_item.getTitle(), "Select an Option ", "Reupload", "Cancel");
		    	}
		    	else if((int) Integer.parseInt(_item.getServerID())==-1){ //audit yet to be uploaded
		    		((CompletedAuditListFragment)_context).serverID = _item.getDb_id(); //getDBIndex
		    		((CompletedAuditListFragment)_context).newDialog(_item.getTitle(), "Select an Option ", "Reupload", "Cancel");
		    	}
		    	else {
		    		((CompletedAuditListFragment)_context).serverID = _item.getDb_id(); //getDBIndex
		    		((CompletedAuditListFragment)_context).newDialog(_item.getTitle(), "No Data available for reuploading - this audit was done on an earlier version of the app" + _item.getTitle(), "Ok", "Cancel");
		    	}
		    }
		});
		
		return row;
	}
}
