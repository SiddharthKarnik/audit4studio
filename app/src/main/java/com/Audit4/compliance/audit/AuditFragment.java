package com.Audit4.compliance.audit;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.ViewAnimator;

import com.Audit4.compliance.CompletedQuestion;
import com.Audit4.compliance.GLOBAL_SETTINGS;
import com.Audit4.compliance.R;
import com.Audit4.compliance.auditsummary.Summary;
import com.Audit4.compliance.contentprovider.VendingContentProvider;
import com.Audit4.compliance.dialogfragments.AddNoteDialogFragment;
import com.Audit4.compliance.dialogfragments.GenericDialogFragment;

import java.util.ArrayList;

public class AuditFragment extends Fragment {

    private VendingContentProvider vcp = new VendingContentProvider();

    private boolean DEBUG = false;

    public static int questionNumber = 0;
    private int tempQuestionNumber = 0;
    public static boolean editMode = false;
    private String auditName, machineID, routeID, Template, Client, ClientSite;

    int TemplateID;
    private String TAG = "AuditFragment";

    private TextView vSection;
    private TextView vQuestion;
    private RadioGroup vRadioGroup;
    private RadioButton vAnsGreen;
    private RadioButton vAnsAmber;
    private RadioButton vAnsRed;
    private TextView vNotes;
    private LinearLayout gallery, additionalNotes;

    private ViewAnimator viewAnim;
    private Animation inAnimNext;
    private Animation outAnim;
    private Animation inAnimPrevious;
    private Animation FadeIn;

    private String[] imagePaths;

    private ArrayList<CompletedQuestion> completedQuestions = new ArrayList<CompletedQuestion>(); //list to store all completed questions

    public static Activity auditActivity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle b = getActivity().getIntent().getExtras();
        auditName = b.getString("Template");
        routeID = b.getString("RouteID");
        machineID = b.getString("MachineID");
        Template = b.getString("Template");
        TemplateID = b.getInt("TemplateID", 0);
        Client = b.getString("Client");
        ClientSite = b.getString("ClientSite");

        auditActivity = this.getActivity();

        imagePaths = new String[7];

        setActionBar();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_audit2, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        vSection = (TextView) getView().findViewById(R.id.auditSectionTextView);
        vQuestion = (TextView) getView().findViewById(R.id.auditQuestionTextView);
        vAnsGreen = (RadioButton) getView().findViewById(R.id.auditAnswer3);
        vAnsAmber = (RadioButton) getView().findViewById(R.id.auditAnswer2);
        vAnsRed = (RadioButton) getView().findViewById(R.id.auditAnswer1);
        vNotes = (TextView) getView().findViewById(R.id.auditNotesTextView);
        vRadioGroup = (RadioGroup) getView().findViewById(R.id.auditAnswerGroup);
        gallery = (LinearLayout) getView().findViewById(R.id.mygallery);

        viewAnim = (ViewAnimator) getView().findViewById(R.id.viewAnimator1);
        additionalNotes = (LinearLayout) getView().findViewById(R.id.additionalNotes);
        additionalNotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (answerHasBeenEntered()) {
                    showNotepad();
                } else
                    showAlertDialog("notes");
            }
        });
        questionNumber = 0;
        editMode = false;

        Fragment questionFragment = (Fragment) getFragmentManager().findFragmentById(R.id.fragment01);
        ((AuditDetailsFragment) questionFragment).setQuestionText(questionNumber);

        setAnswers();
        setActionBar();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (tempQuestionNumber != questionNumber) refresh();
    }


    @Override
    public void onPause() {
        super.onPause();
        tempQuestionNumber = questionNumber;
    }

    public void setActionBar() {
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(auditName + ": " + (questionNumber + 1) + " of " + Audit.questionList.size());
    }

    public void moveToNextQuestion() {
        if (questionNumber + 1 < Audit.questionList.size() && editMode == false) {
            if (questionHasBeenAnswered()) overwriteAnswer();
            else saveAnswer();

            questionNumber++;
            nextAnim();
            refresh();
        } else {
            if (questionHasBeenAnswered()) overwriteAnswer();
            else saveAnswer();

            //if audit is empty throw warning
            if (auditIsEmpty()) showAlertDialog("noQuestionsAnswered");
            else {
                auditComplete();
            }
        }
    }

    public void moveToPreviousQuestion() {
        if (editMode == true) {
            auditComplete();
        } else {
            if (questionNumber > 0) {
                if (questionHasBeenAnswered()) overwriteAnswer();
                else saveAnswer();
                questionNumber--;
                previousAnim();
                refresh();
            } else {
                if (GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d(TAG, "Start");
            }
        }
    }

    public void nextAnim() {
        if (GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d("Width", " " + viewAnim.getWidth());

        //inAnimNext = new TranslateAnimation(viewAnim.getWidth()*1.2f, 0, 0, 0);
        //inAnimNext.setDuration(1500);
        inAnimNext = new AlphaAnimation(0, 1);
        inAnimNext.setDuration(1500);

        outAnim = new AlphaAnimation(1, 0);
        outAnim.setDuration(2000);

        viewAnim.setInAnimation(inAnimNext);
        viewAnim.setOutAnimation(outAnim);
        viewAnim.showNext();
    }

    public void previousAnim() {
        FadeIn = outAnim = new AlphaAnimation(0, 1);
        ;
        FadeIn.setDuration(1000);
        //inAnimPrevious = new  TranslateAnimation(-viewAnim.getWidth(), 0, 0, 0);
        inAnimPrevious = new AlphaAnimation(1, 0);

        viewAnim.setInAnimation(FadeIn);
        viewAnim.setOutAnimation(inAnimPrevious);
        viewAnim.showNext();
    }

    void auditComplete() {
        Intent moveToSummary = new Intent(getActivity(), Summary.class);
        //moveToSummary.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        moveToSummary.putExtra("MachineID", machineID);
        moveToSummary.putExtra("auditName", auditName);
        moveToSummary.putExtra("Template", Template);
        moveToSummary.putExtra("routeID", routeID);
        moveToSummary.putExtra("Client", Client);
        moveToSummary.putExtra("ClientSite", ClientSite);
        moveToSummary.putExtra("TemplateID", TemplateID);
        Log.d("PAssing", "" + TemplateID);

        //moveToSummary.putExtra("auditID", auditID);
        moveToSummary.putExtra("completedQuestions", completedQuestions);

        if (GLOBAL_SETTINGS.DEBUG || this.DEBUG)
            Log.d("FINSIHED", "machine Id: " + machineID + " Audit Name: " + auditName + " Template: " + Template);
        ActivityOptions opts = ActivityOptions.makeCustomAnimation(getActivity().getBaseContext(),
                R.anim.fade_in, R.anim.slide_out_left);
        startActivity(moveToSummary, opts.toBundle());
        //getActivity().finish();
    }

    void refresh() {
        resetImages();
        setActionBar();
        Fragment questionFragment = (Fragment) getFragmentManager().findFragmentById(R.id.fragment01);
        ((AuditDetailsFragment) questionFragment).setQuestionText(questionNumber);
        setAnswers();
        setAnswerDetails();
    }

    //OLD CODE BELOW ---------------

    void setAnswers() {
        String[] answers = new String[3];
        answers = Audit.questionList.get(questionNumber).getAnswers();

        if (GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d("AmberAnswer", answers[1]);
        if (answers[1].equals("N/A.")) {
            vAnsAmber.setVisibility(View.GONE);
        } else {
            vAnsAmber.setVisibility(View.VISIBLE);
        }


        vAnsRed.setText(answers[0]);
        vAnsAmber.setText(answers[1]);
        vAnsGreen.setText(answers[2]);
    }

    /* METHODS TO ADD IMAGES TO THE GALLERY */
    public void addPathToArray(String imagePath) {
        if (GLOBAL_SETTINGS.DEBUG || this.DEBUG)
            Log.d(TAG, "addPathToArray: image path to add = " + imagePath);

        //CHECK IF EMPTY PATHS ARE AVAILABLE
        int emptyPaths = 0;
        for (int i = 0; i < imagePaths.length; i++) {
            if (imagePaths[i] == null) emptyPaths++;
            if (GLOBAL_SETTINGS.DEBUG || this.DEBUG)
                Log.d(TAG, "addPathToArray: path = " + imagePaths[i]);
        }

        if (GLOBAL_SETTINGS.DEBUG || this.DEBUG)
            Log.d(TAG, "addPathToArray: empty paths = " + emptyPaths);
        //ADD PATH TO FIRST AVALABLE INDEX
        if (emptyPaths > 0) {
            int index = imagePaths.length - emptyPaths;
            if (GLOBAL_SETTINGS.DEBUG || this.DEBUG)
                Log.d(TAG, "addPathToArray: adding path to index imagePaths[" + index + "] and creating image view");
            imagePaths[index] = imagePath;
            addImageToGallery(imagePath);
        } else {
            ((Audit) getActivity()).showAlertDialog("noImageSpaceAvailable");
            if (GLOBAL_SETTINGS.DEBUG || this.DEBUG)
                Log.d(TAG, "addPathToArray: no paths available");
        }
    }

    public void addImageToGallery(String imagePath) {
        gallery.addView(insertPhoto(imagePath));
        if (GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d(TAG, "addImageToGallery: add image to view");
    }

    View insertPhoto(String path) {
        if (GLOBAL_SETTINGS.DEBUG || this.DEBUG)
            Log.d(TAG, "insertPhoto: insertingPhoto into gallery");
        Bitmap bm = decodeSampledBitmapFromUri(path, 220, 220);

        LinearLayout layout = new LinearLayout(getActivity().getApplicationContext());
        layout.setLayoutParams(new LayoutParams(250, 250));
        layout.setGravity(Gravity.CENTER);

        ImageView imageView = new ImageView(getActivity().getApplicationContext());
        imageView.setLayoutParams(new LayoutParams(220, 220));
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setImageBitmap(bm);

        layout.addView(imageView);
        return layout;
    }

    public Bitmap decodeSampledBitmapFromUri(String path, int reqWidth,
                                             int reqHeight) {
        Bitmap bm = null;

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth,
                reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        bm = BitmapFactory.decodeFile(path, options);

        return bm;
    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round((float) height / (float) reqHeight);
            } else {
                inSampleSize = Math.round((float) width / (float) reqWidth);
            }
        }
        return inSampleSize;
    }
/* METHODS TO ADD IMAGES TO THE GALLERY */

    public void setAnswersForQuestion() {
        String[] answers = new String[3];
        //	answers = vcp.getAnswer(questionNumber, auditID);

        vAnsRed.setText(answers[0]);
        vAnsAmber.setText(answers[1]);
        vAnsGreen.setText(answers[2]);
    }

    public void setNotes(String Notes) {
        vNotes.setText(Notes);
    }

    public void resetImages() {
        imagePaths = new String[7];
        gallery.removeAllViews();
        //clear view
    }

    public void showNotepad() {
        String currentNotes = vNotes.getText().toString();
        AddNoteDialogFragment fragment = new AddNoteDialogFragment();
        fragment.setAddNoteDialogFragment("Additional Notes", currentNotes, "Add", "Cancel"); //Pass current text
        fragment.show(getActivity().getSupportFragmentManager(), "Note");
    }

    public boolean questionHasBeenAnswered() {
        if (questionNumber + 1 > completedQuestions.size()) return false;
        else return true;
    }

    public void overwriteAnswer() {
        completedQuestions.get(questionNumber).setAnswerGrade(getSelectedAnswer());
        completedQuestions.get(questionNumber).setAdditionalNotes(vNotes.getText().toString());
        completedQuestions.get(questionNumber).setImagePaths(imagePaths);
    }

    public String getSelectedAnswer() {
        if (vAnsRed.isChecked()) return "L";
        else if (vAnsAmber.isChecked()) return "M";
        else if (vAnsGreen.isChecked()) return "H";
        else return "NONE";
    }

    public void saveAnswer() {
        int TemplateAuditAnswerID = Audit.questionList.get(questionNumber).getTemplateAuditQuestionID();
        int CompanyAuditAnswerID = Audit.questionList.get(questionNumber).getCompanyAuditQuestionID();

        int questionRank = Audit.questionList.get(questionNumber).getQuestionRank();

        completedQuestions.add(new CompletedQuestion(questionNumber, questionRank, getSelectedAnswer(), vNotes.getText().toString(), imagePaths, TemplateAuditAnswerID, CompanyAuditAnswerID));
    }

    public boolean answerHasBeenEntered() {
        if (vRadioGroup.getCheckedRadioButtonId() == -1) return false;
        else return true;
    }

    public void setAnswerDetails() {
        if (questionHasBeenAnswered()) {
            if (completedQuestions.get(questionNumber).getAnswerGrade().equals("L"))
                vAnsRed.setChecked(true);
            else if (completedQuestions.get(questionNumber).getAnswerGrade().equals("M"))
                vAnsAmber.setChecked(true);
            else if (completedQuestions.get(questionNumber).getAnswerGrade().equals("H"))
                vAnsGreen.setChecked(true);
            else vRadioGroup.clearCheck();

            vNotes.setText(completedQuestions.get(questionNumber).getAdditionalNotes());

            imagePaths = completedQuestions.get(questionNumber).getImagePaths();

            //ADD IMAGE IF PATH ISNT NULL
            for (int i = 0; i < imagePaths.length; i++) {
                if (imagePaths[i] != null) addImageToGallery(imagePaths[i]);
            }
        } else {
            vRadioGroup.clearCheck();
            vNotes.setText("");
        }
    }

    public boolean auditIsEmpty() {
        int answeredQuestions = 0;
        for (CompletedQuestion cq : completedQuestions) {
            if (!cq.getAnswerGrade().equals("NONE")) answeredQuestions++;
        }
        if (answeredQuestions > 0) return false;
        else return true;
    }

    public void showAlertDialog(String dialogType) {
        String title = null;
        String message = null;
        String button1 = null;
        String button2 = null;

        switch (dialogType) {
            case "noImageSpaceAvailable":
                Audit.dialogType = "noImageSpaceAvailable";
                title = "Audit";
                message = "You have taken the maximum amount of images for this question!";
                button1 = "Continue";
                button2 = "";
                break;
            case "noQuestionsAnswered":
                Audit.dialogType = "noQuestionsAnswered";
                title = "Audit";
                message = "You haven't answered any questions, questions are required to be answered in order for an audit to be valid!";
                button1 = "Continue";
                button2 = "";
                break;
            case "notes":
                Audit.dialogType = "notes";
                title = "Notes";
                message = "Please select an Answer before adding notes";
                button1 = "Continue";
                button2 = "";
                break;
        }
        GenericDialogFragment newFragment = new GenericDialogFragment();
        newFragment.setGenericDialogFragment(title, message, button1, button2);
        newFragment.show(getFragmentManager(), "alert");
    }
}
