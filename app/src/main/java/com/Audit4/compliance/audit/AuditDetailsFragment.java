package com.Audit4.compliance.audit;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.Audit4.compliance.contentprovider.VendingContentProvider;
import com.Audit4.compliance.R;

public class AuditDetailsFragment extends Fragment {
	private VendingContentProvider vcp = new VendingContentProvider();
	private TextView vSection;
	private TextView vQuestionTitle;
	private TextView vQuestion;
	private RadioButton vAnsGreen;
	private RadioButton vAnsAmber;
	private RadioButton vAnsRed;
	
	private int AuditID;
	private String answers[] = new String[3];
	
	private boolean DEBUG = false;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View view = inflater.inflate(R.layout.fragment_audit2_details, container, false);
	        
		vSection = (TextView) view.findViewById(R.id.section);
		vQuestionTitle = (TextView) view.findViewById(R.id.questionTitle);
		vQuestion = (TextView) view.findViewById(R.id.question);
		
		
		return view;
	}
	    
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}
	
	public void setQuestionText(int questionNumber) {
		
		String question = Audit.questionList.get(questionNumber).getQuestion();
		String questionTitle = Audit.questionList.get(questionNumber).getTitle();
		int Section = Audit.questionList.get(questionNumber).getSection();
		String SectionName = vcp.getSectionName(Section);

		vQuestionTitle.setText(questionTitle);
		vQuestion.setText(question);
		vSection.setText(SectionName);
	}

}

