package com.Audit4.compliance.audit;

import android.Manifest;
import android.app.ActivityOptions;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;
import android.media.ExifInterface;
import android.graphics.Matrix;

import com.Audit4.compliance.ActionBar_Bottom.buttonListener;
import com.Audit4.compliance.GLOBAL_SETTINGS;
import com.Audit4.compliance.R;
import com.Audit4.compliance.dialogfragments.AddNoteDialogFragment;
import com.Audit4.compliance.dialogfragments.GenericDialogFragment;
import com.Audit4.compliance.utils.Util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Audit extends AppCompatActivity implements GenericDialogFragment.NoticeDialogListener, buttonListener, AddNoteDialogFragment.NoticeDialogListener {
    private static String TAG = "Audit";
    public String auditName;
    static String dialogType;
    private boolean ignoreWarning;
    private Snackbar snackbar;
    private boolean DEBUG = true;
    View mLayout;
    public static ArrayList<QuestionList> questionList = new ArrayList<QuestionList>();


    /* CAMERA INTENT CONSTANTS */
    private File photoFile = null;
    String mCurrentPhotoPath;
    static final int REQUEST_TAKE_PHOTO = 1;
    private static String imagePath = "";
    /* CAMERA INTENT CONSTANTS */

    @Override
    public void onDialogPositiveClick(AddNoteDialogFragment dialog) {
        Fragment fragment = (Fragment) getSupportFragmentManager().findFragmentById(R.id.TemplateList);
        ((AuditFragment) fragment).setNotes(AddNoteDialogFragment.currentText);
    }

    @Override
    public void onDialogNegativeClick(AddNoteDialogFragment dialog) {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        auditName = getIntent().getStringExtra("auditName");
        //setContentView(R.layout.pi_activity_audit);
        setContentView(R.layout.activity_audit);
        mLayout = findViewById(R.id.container);

    }


    @Override
    public Intent getParentActivityIntent() {
        onBackPressed();
        return null;
    }

    @Override
    public void buttonClicked(String which) {
        Fragment fragment = (Fragment) getSupportFragmentManager().findFragmentById(R.id.TemplateList);

        switch (which) {
            case "A":
                ((AuditFragment) fragment).moveToPreviousQuestion();
                break;
            case "B":
                //Add notes
                if (((AuditFragment) fragment).answerHasBeenEntered()) {
                    ((AuditFragment) fragment).showNotepad();
                } else
                    showAlertDialog("notes");
                break;
            case "C":
                if (GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d(TAG, "LaunchCamera: button pressed");
                if (((AuditFragment) fragment).answerHasBeenEntered()) {
                    // dispatchTakePictureIntent();
                    checkCameraPermission();
                } else {
                    showAlertDialog("camera");
                }
                break;
            case "D":
                //	((AuditFragment) fragment).moveToNextQuestion();
                if (((AuditFragment) fragment).answerHasBeenEntered() && ignoreWarning == false)
                    ((AuditFragment) fragment).moveToNextQuestion();
                else {

                    showAlertDialog("unansweredQuestion");
                }
                break;
            default:
                if (GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d("CLICK", "INVALID");
                break;
        }
    }

    private void checkCameraPermission() {
        boolean allPermissionGranted = Util.checkPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, Util.REQ_CODE_LOC_CAM, Audit.this);

        if (allPermissionGranted) {
            dispatchTakePictureIntent();
        }
    }

    @Override
    public void onBackPressed() {
        showAlertDialog("onBackPressed");
    }

    public void showAlertDialog(String dialogType) {
        String title = null;
        String message = null;
        String button1 = null;
        String button2 = null;

        switch (dialogType) {
            case "onBackPressed":
                this.dialogType = "onBackPressed";
                title = "Audit";
                message = "Are you sure you wish to end this audit? All current progress will be lost!";
                button1 = "End Audit";
                button2 = "Cancel";
                break;
            case "unansweredQuestion":
                this.dialogType = "unansweredQuestion";
                title = "Audit";
                message = "You haven't entered an answer for this question, please ensure the question is not applicable for the asset being audited before moving on!";
                button1 = "Continue";
                button2 = "Cancel";
                break;
            case "camera":
                this.dialogType = "camera";
                title = "Camera";
                message = "Please select an Answer before taking an photo";
                button1 = "Continue";
                button2 = "";
                break;
            case "notes":
                this.dialogType = "notes";
                title = "Notes";
                message = "Please select an Answer before adding notes";
                button1 = "Continue";
                button2 = "";
                break;

        }
        GenericDialogFragment newFragment = new GenericDialogFragment();
        newFragment.setGenericDialogFragment(title, message, button1, button2);
        newFragment.show(getSupportFragmentManager(), "alert");
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        switch (dialogType) {
            case "onBackPressed":
                finish();
                break;
            case "unansweredQuestion":
                Fragment fragment = (Fragment) getSupportFragmentManager().findFragmentById(R.id.TemplateList);
                ((AuditFragment) fragment).moveToNextQuestion();
                break;
            case "noImageSpaceAvailable":
                dialog.dismiss();
                break;
            case "noQuestionsAnswered":
                dialog.dismiss();
                break;
            default:
                dialog.dismiss();
                if (GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d("POSITIVE", "CLICK");
                break;
        }
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
        //dismiss
    }

    /* USED BY THE CAMERA INTENT TO CREATE AN IMAGE FILE */
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        if (GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d("StorageDir", " " + storageDir);
        File image = File.createTempFile(
                imageFileName,  // prefix
                ".jpg",         // suffix
                storageDir      // directory
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        if (GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d(TAG, "createImageFile: imageFileCreated");
        return image;
    }
    // USED BY THE CAMERA INTENT TO CREATE AN IMAGE FILE

    // LAUNCH THE CAMERA INTENT
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        ActivityOptions opts = ActivityOptions.makeCustomAnimation(this, R.anim.fade_in, R.anim.slide_out_left);
        if (GLOBAL_SETTINGS.DEBUG || this.DEBUG)
            Log.d(TAG, "dispatchTakePictureIntent: camera launched");

        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                ex.printStackTrace();
                Toast.makeText(this, "Error Creating File", Toast.LENGTH_SHORT).show();
                if (GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d("EXPECTION", " " + ex);
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                int currentapiVersion = Build.VERSION.SDK_INT;
                Uri intentUri;
                if (currentapiVersion >= 24) {
                    intentUri = FileProvider.getUriForFile(Audit.this, getApplicationContext().getPackageName() + ".provider", photoFile);
                } else {
                    intentUri = Uri.fromFile(photoFile);
                }
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        intentUri);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO, opts.toBundle());
            }
        }
    }
    // LAUNCH THE CAMERA INTENT

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    // RETURNS THE IMAGE TO THE ACTIVITY
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
            imagePath = photoFile.getPath(); //get path name
            if (GLOBAL_SETTINGS.DEBUG || this.DEBUG)
                Log.d(TAG, "onActivityResult: image path returned = " + imagePath);
            Bitmap bmp = BitmapFactory.decodeFile(photoFile.getAbsolutePath()); //create a bitmap image

            ExifInterface ei = null;
            int orientation = 0;
            try {
                ei = new ExifInterface(photoFile.getAbsolutePath());
                orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_UNDEFINED);
            } catch (IOException e) {
                e.printStackTrace();
            }

            Bitmap resized = null;
            resized = Bitmap.createScaledBitmap(bmp, GLOBAL_SETTINGS.PHOTO_WIDTH, GLOBAL_SETTINGS.PHOTO_HEIGHT, true);

            switch(orientation) {

                case ExifInterface.ORIENTATION_ROTATE_90:
                    resized = rotateImage(resized, 90);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_180:
                    resized = rotateImage(resized, 180);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_270:
                    resized = rotateImage(resized, 270);
                    break;

                case ExifInterface.ORIENTATION_NORMAL:

                default:
                    break;
            }


            /*
            if (bmp.getHeight() < bmp.getWidth()) {
                resized = Bitmap.createScaledBitmap(bmp, GLOBAL_SETTINGS.PHOTO_WIDTH, GLOBAL_SETTINGS.PHOTO_HEIGHT, true);
            } //Horizontal
            else {
                resized = Bitmap.createScaledBitmap(bmp, GLOBAL_SETTINGS.PHOTO_HEIGHT, GLOBAL_SETTINGS.PHOTO_WIDTH, true);
            } //Vertical
            */

            //
           // resized = Bitmap.createScaledBitmap(bmp, GLOBAL_SETTINGS.PHOTO_HEIGHT, GLOBAL_SETTINGS.PHOTO_WIDTH, true);

            ByteArrayOutputStream blob = new ByteArrayOutputStream();
            resized.compress(Bitmap.CompressFormat.JPEG, 100, blob);
            FileOutputStream stream;
            try {
                stream = new FileOutputStream(photoFile.getPath());
                stream.write(blob.toByteArray());
            } catch (FileNotFoundException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


            try {
                android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
                Fragment frag = (Fragment) fm.findFragmentById(R.id.TemplateList);
                ((AuditFragment) frag).addPathToArray(imagePath); //pass the path to the fragment
            } catch (Exception e) {

            }
        } else if (resultCode == RESULT_CANCELED) {
            Toast.makeText(this, "Capture canceled!", Toast.LENGTH_SHORT).show();
        } else if (resultCode == Util.REQUEST_PERMISSION_SETTING) {
            checkCameraPermission();
        } else Toast.makeText(this, "Capture failed!", Toast.LENGTH_SHORT).show();
    }
    // RETURNS THE IMAGE TO THE ACTIVITY

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        Log.e("onRequestPermissionsRes", "called");
        AlertDialog alert;
        AlertDialog.Builder builder = new AlertDialog.Builder(Audit.this);
        switch (requestCode) {
            case Util.REQ_CODE_LOC_CAM:

                if (grantResults.length > 0) {
                    boolean allGranted = true;
                    boolean rationaliGranted = true;
                    for (int i = 0; i < permissions.length; i++) {
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {

                            //Permission Granted Successfully. Write working code here.
                            allGranted = false;
                            if (!shouldShowRequestPermissionRationale(permissions[i])) {
                                // user denied flagging NEVER ASK AGAIN
                                // you can either enable some fall back,
                                // disable features of your app
                                // or open another dialog explaining
                                // again the permission and directing to
                                // the app setting
                                rationaliGranted = false;

                            }
                        }
                    }
                    if (allGranted) {
                        dispatchTakePictureIntent();
                    } else if (!rationaliGranted) {

                        builder.setTitle(R.string.app_settings);
                        builder.setMessage(R.string.permission_needed);
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package", getPackageName(), null);
                                intent.setData(uri);
                                startActivityForResult(intent, Util.REQUEST_PERMISSION_SETTING);
                            }
                        });

                        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();

                            }
                        });
                        alert = builder.create();

                        if (!alert.isShowing()) {
                            try {
                                alert.show();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                    } else {
                        snackbar = Snackbar.make(mLayout, R.string.permission_camera_and_storage_is_needed,
                                Snackbar.LENGTH_SHORT).setAction(R.string.app_settings, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package", getPackageName(), null);
                                intent.setData(uri);
                                startActivityForResult(intent, Util.REQUEST_PERMISSION_SETTING);
                            }
                        });
                        snackbar.show();
                    }
                }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return true;
    }
}