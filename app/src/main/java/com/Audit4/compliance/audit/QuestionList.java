package com.Audit4.compliance.audit;

public class QuestionList {
	
	private String QuestionTitle;
	private int QuestionRank;
	private String Question;
	private int Section;
	private String rAnswer;
	private String aAnswer;
	private String gAnswer;
	private int TemplateAuditQuestionID;
	private int CompanyAuditQuestionID;
	
	public void setQuestions(String QuestionTitle, int QuestionRank, String Question, int Section, String rAnswer, String aAnswer, String gAnswer, int TemplateAuditQuestionID, int CompanyAuditQuestionID)
	{
		this.QuestionTitle = QuestionTitle;
		this.Question = Question;
		this.QuestionRank = QuestionRank;
		this.Section = Section;
		this.rAnswer = rAnswer;
		this.aAnswer = aAnswer;
		this.gAnswer = gAnswer;
		this.TemplateAuditQuestionID = TemplateAuditQuestionID;
		this.CompanyAuditQuestionID = CompanyAuditQuestionID;
	}
	
	
	public String getTitle()
	{
		return QuestionTitle;
	}
	
	public String getQuestion()
	{
		return Question;
	}
	
	public int getSection()
	{
		return Section;
	}
	
	public  String[] getAnswers()
	{
		String[] answers = new String[3];
		answers[0] = rAnswer;
		answers[1] = aAnswer;
		answers[2] = gAnswer;
		
		return answers;
	}

	public int getTemplateAuditQuestionID()
	{
		return TemplateAuditQuestionID;
	}
	
	public int getCompanyAuditQuestionID()
	{
		return CompanyAuditQuestionID;
	}
	
	public int getQuestionRank()
	{
		return QuestionRank;
	}
}
