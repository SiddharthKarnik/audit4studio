package com.Audit4.compliance;

public class AuditItem {
	private String machineID = null;
	private String date = null;
	private String auditorName = null;
	private boolean uploaded = false;
	private String serverID = null;
	private int db_id;

	public AuditItem() {
	}
	
	public AuditItem(String machineID, String date, String auditorName, boolean uploaded, String id) {
		this.machineID = machineID;
		this.date = date;
		this.auditorName = auditorName;
		this.uploaded = uploaded;
		this.serverID = id;
	}
	
	public AuditItem(String machineID, String date, String auditorName, boolean uploaded, String id, int db_id) {
		this.machineID = machineID;
		this.date = date;
		this.auditorName = auditorName;
		this.uploaded = uploaded;
		this.serverID = id;
		this.setDb_id(db_id);
	}

	public void setTitle(String title) {
		machineID = title;
	}

	public void setDescription(String description) {
		date = description;
	}

	public void setLink(String link) {
		auditorName = link;
	}
	
	public void setServerID(String id){
		serverID = id;
	}

	public String getTitle() {
		return machineID;
	}

	public String getDescription() {
		return date;
	}

	public String getLink() {
		return auditorName;
	}
	
	public String getServerID(){
		return serverID;
	}

	public String toString() {
		// limit how much text you display
		if (machineID.length() > 42) {
			return machineID.substring(0, 42) + "...";
		}
		return machineID;
	}

	public boolean isUploaded() {
		return uploaded;
	}

	public void setUploaded(boolean uploaded) {
		this.uploaded = uploaded;
	}

	public int getDb_id() {
		return db_id;
	}

	public void setDb_id(int db_id) {
		this.db_id = db_id;
	}

}
