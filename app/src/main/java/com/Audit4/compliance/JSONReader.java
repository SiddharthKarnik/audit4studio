package com.Audit4.compliance;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.Audit4.compliance.contentprovider.VendingContentProvider;
import com.Audit4.compliance.login.Login;
import com.Audit4.compliance.utils.Webservice;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class JSONReader extends AsyncTask<String, Integer, String> {

    private boolean DEBUG = true;

    private String TAG = "JSONReader";
    private Activity mActivity;
    private Fragment frag;
    private String API; //example - getQuestionType etc
    private String foreginKeyName; //example userID, routeID, machineID etc
    private String foreginKeyValue;

    static Intent activityIntent;

    private VendingContentProvider vcp;
    private boolean startHomeActivity;



    public JSONReader(Activity activity, Fragment frag, String API, String foreginKeyName, String foreginKeyValue) {
        this.mActivity = activity;
        this.frag = frag;
        this.API = API;
        this.foreginKeyValue = foreginKeyValue;
        this.foreginKeyName = foreginKeyName;
    }
    public JSONReader(Activity activity, Fragment frag, String API, String foreginKeyName, String foreginKeyValue, boolean startHomeActivity) {
        this.mActivity = activity;
        this.frag = frag;
        this.API = API;
        this.foreginKeyValue = foreginKeyValue;
        this.foreginKeyName = foreginKeyName;
        this.startHomeActivity = startHomeActivity;
    }

    //Main runner
    public static void Populate(Activity activity, Intent mIntent) {
        activityIntent = mIntent;

        new JSONReader(activity, null, "getQuestionRanks", "questionRanks", "").execute(); //Populates Question Ranks into the local Database
        new JSONReader(activity, null, "getQuestionTypes", "questionTypes", "").execute();  //Populates Question Types into the local Database

        //Sets Last Downloaded in the Preferences
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activity.getApplicationContext());
        SharedPreferences.Editor editor = prefs.edit();
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        editor.putString("LastDownLoad", df.format(c.getTime()));
        editor.commit();

        String ForeignKey = prefs.getString("CompanyID", "null");
        new JSONReader(activity, null, "getCompanyAudits", "companyID", ForeignKey).execute();
    }

    //Pull back data from server from appropriate api
    @Override
    protected String doInBackground(String... parms)

    {
        InputStream inputStream = null;
        StringBuilder stringBuilder = null;
        String result = null;
        result = Webservice.Populate(WebsiteUrl.webURl + "/includes/APIS/" + API + ".php", foreginKeyName, foreginKeyValue);

        return result;
    }

    @Override
    protected void onPostExecute(String result) {
        //Depending on which API was read - an action is taken after the result has been collected from the server
        switch (API) {
            case "getQuestionRanks":
                QuestionRanks(result);
                break;
            case "getQuestionTypes":
                QuestionTypes(result);
                break;
            case "getCompanyAudits":
                CompanyAudits(result);
                break;
            case "getTemplateAudit":
                TemplateAudit(result);
                break;
            case "getTemplateAuditAnswers":
                TemplateAuditAnswers(result);
                break;
            case "getCompanyAuditAnswers":
                CompanyAuditAnswers(result);
                break;
        }
    }

    //Parses the data from the Question Ranks API
    void QuestionRanks(String result) {
        JSONArray jArray = null;

        try {
            jArray = new JSONArray(result);
            JSONObject json_data = null;

            //TODO if error .... else

            for (int i = 0; i < jArray.length(); i++) {
                json_data = jArray.getJSONObject(i);

                //Variables parsed from json
                int QuestionRankID = json_data.getInt("QuestionRankID");
                String QuestionRank = json_data.getString("QuestionRank");
                int RedScore = json_data.getInt("RedScore");
                int AmberScore = json_data.getInt("AmberScore");
                int GreenScore = json_data.getInt("GreenScore");

                //Inserts into the VCP
                vcp = new VendingContentProvider();
                vcp.insertQuestionRank(QuestionRankID, QuestionRank, RedScore, AmberScore, GreenScore);
            }
        } catch (Exception e) {
            if (GLOBAL_SETTINGS.DEBUG || this.DEBUG)
                Log.e(TAG, "QuestionRanks: Error " + e.toString());
        }
    }

    //Parses the data from the Question Ranks API
    void QuestionTypes(String result) {
        {
            JSONArray jArray = null;

            try {
                jArray = new JSONArray(result);
                JSONObject json_data = null;

                //TODO if error .... else

                for (int i = 0; i < jArray.length(); i++) {
                    json_data = jArray.getJSONObject(i);

                    //Variables parsed from json
                    int QuestionID = json_data.getInt("QuestionTypeID");
                    String QuestionType = json_data.getString("QuestionType");
                    int NumberOfAnswers = json_data.getInt("NumberOfAnswers");

                    //Inserts into the VCP
                    vcp = new VendingContentProvider();
                    vcp.insertQuestionType(QuestionID, QuestionType, NumberOfAnswers);
                }
            } catch (Exception e) {
                if (GLOBAL_SETTINGS.DEBUG || this.DEBUG)
                    Log.e(TAG, "QuestionTypes: Error " + e.toString());
            }
        }
    }

    //Parses the data from the Company Audit API - Then runs getTemplateAudit, TemplateAuditAnswers and CompanyAuditAnswers
    void CompanyAudits(String result) {
        JSONArray jArray = null;

        if (result.contains("No data in table")) {
            if (GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d("ERROR", "No Data in table");
            ((Login) mActivity).hideProgressBar();
            ((Login) mActivity).noAuditsAssigned();
        } else {
            try {
                jArray = new JSONArray(result);
                JSONObject json_data = null;


                //TODO if error .... else
                for (int i = 0; i < jArray.length(); i++) {
                    json_data = jArray.getJSONObject(i);

                    //Variables parsed from json
                    int CompanyAuditID = json_data.getInt("CompanyAuditID");
                    String CompanyAuditName = json_data.getString("CompanyAuditName");
                    int CompanyAuditBenchmark = json_data.getInt("CompanyAuditBenchmark");
                    int TemplateAuditID = json_data.getInt("TemplateAuditID");


                    //Inserts into the VCP
                    vcp = new VendingContentProvider();
                    vcp.insertCompanyAudit(CompanyAuditID, CompanyAuditName, CompanyAuditBenchmark, TemplateAuditID);

                    //Add TemplateAudit
                    new JSONReader(mActivity, frag, "getTemplateAudit", "templateAuditID", Integer.toString(TemplateAuditID)).execute();
                    new JSONReader(mActivity, frag, "getTemplateAuditAnswers", "templateAuditID", Integer.toString(TemplateAuditID)).execute();
                    new JSONReader(mActivity, frag, "getCompanyAuditAnswers", "companyAuditID", Integer.toString(CompanyAuditID), (i == jArray.length() - 1)).execute();

                }
            } catch (Exception e) {
                if (GLOBAL_SETTINGS.DEBUG || this.DEBUG)
                    Log.e(TAG, "CompanyAudit: Error " + e.toString());
            }
        }
    }

    //Parses the data from Template Audit API
    void TemplateAudit(String result) {
        JSONArray jArray = null;

        try {
            jArray = new JSONArray(result);
            JSONObject json_data = null;

            //TODO if error .... else

            for (int i = 0; i < jArray.length(); i++) {
                json_data = jArray.getJSONObject(i);

                //Variables parsed from json
                int TemplateAuditID = json_data.getInt("TemplateAuditID");
                String TemplateAuditName = json_data.getString("TemplateAuditName");
                int AuditType = json_data.getInt("AuditAssetTypeID");

                //Inserts into the VCP
                vcp = new VendingContentProvider();
                vcp.insertTemplateAudit(TemplateAuditID, TemplateAuditName, AuditType);
            }
        } catch (Exception e) {
            if (GLOBAL_SETTINGS.DEBUG || this.DEBUG)
                Log.e(TAG, "TemplateAudit: Error " + e.toString());
        }

    }

    //Parses the data from Template Audit Answers Audit API
    void TemplateAuditAnswers(String result) {
        int TemplateAuditID = -1;

        try {

            JSONObject Result = new JSONObject(result);

            //Audit Section Details
            JSONArray Audit = Result.getJSONArray("AuditSections");

            for (int i = 0; i < Audit.length(); i++) {
                JSONObject tempData = Audit.getJSONObject(i);

                int TemplateAuditQuestionSectionID = tempData.getInt("TemplateAuditQuestionSectionID");
                String TemplateAuditSectionName = tempData.getString("TemplateAuditSectionName");
                TemplateAuditID = tempData.getInt("TemplateAuditID");
                if (GLOBAL_SETTINGS.DEBUG || this.DEBUG)
                    Log.d("TemplateAuditID", " " + TemplateAuditID);


                //Add to Database
                if (GLOBAL_SETTINGS.DEBUG || this.DEBUG)
                    Log.d("TemplateAudit", TemplateAuditQuestionSectionID + TemplateAuditSectionName + TemplateAuditID);
                vcp = new VendingContentProvider();
                vcp.insertTemplateAuditQuestionSection(TemplateAuditQuestionSectionID, TemplateAuditSectionName, TemplateAuditID);
            }

            //Question Details
            JSONArray Questions = Result.getJSONArray("Questions");

            for (int i = 0; i < Questions.length(); i++) {
                JSONObject tempData = Questions.getJSONObject(i);

                int TemplateAuditQuestionID = tempData.getInt("TemplateAuditQuestionID");
                int TemplateAuditQuestionSectionID = tempData.getInt("TemplateAuditQuestionSectionID");
                String TemplateAuditQuestionTitle = tempData.getString("TemplateAuditQuestionTitle");
                String TemplateAuditQuestion = tempData.getString("TemplateAuditQuestion");
                int QuestionRankID = tempData.getInt("QuestionRankID");
                int QuestionTypeID = tempData.getInt("QuestionTypeID");

                //Add to Data Base
                vcp = new VendingContentProvider();
                vcp.insertTemplateAuditQuestion(TemplateAuditQuestionID, TemplateAuditQuestionSectionID, TemplateAuditQuestionTitle, TemplateAuditQuestion, QuestionTypeID, QuestionRankID, TemplateAuditID);

                //Answer Details
                JSONArray Answers = tempData.getJSONArray("Answers");

                for (int j = 0; j < Answers.length(); j++) {
                    JSONObject answerData = Answers.getJSONObject(j);

                    int TemplateAuditAnswerID = answerData.getInt("TemplateAuditAnswerID");
                    String TemplateAuditAnswerGrade = answerData.getString("TemplateAuditAnswerGrade");
                    String TemplateAuditAnswer = answerData.getString("TemplateAuditAnswer");

                    //Add to Data Base
                    vcp = new VendingContentProvider();
                    vcp.insertTemplateAuditAnswer(TemplateAuditAnswerID, TemplateAuditAnswerGrade, TemplateAuditAnswer, TemplateAuditQuestionID);
                }
            }

        } catch (Exception e) {
            if (GLOBAL_SETTINGS.DEBUG || this.DEBUG)
                Log.e(TAG, "TemplateAuditAnswers: Error " + e.toString());
        }
    }

    //Parses the data from Company Audit Answers API - Then proceeds to Login
    void CompanyAuditAnswers(String result) {
        try {
            JSONObject Result = new JSONObject(result);

            if (Result.getString("Questions").equals("no questions available")) {
                if (GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d(TAG, "No Questions");
            } else {
                JSONArray Questions = Result.getJSONArray("Questions");

                for (int i = 0; i < Questions.length(); i++) {
                    JSONObject tempData = Questions.getJSONObject(i);

                    int CompanyAuditQuestionID = tempData.getInt("CompanyAuditQuestionID");
                    String CompanyAuditQuestionTitle = tempData.getString("CompanyAuditQuestionTitle");
                    String CompanyAuditQuestion = tempData.getString("CompanyAuditQuestion");
                    int QuestionTypeID = tempData.getInt("QuestionTypeID");
                    int QuestionRankID = tempData.getInt("QuestionRankID");
                    int CompanyAuditID = tempData.getInt("CompanyAuditID");
                    int TemplateAuditQuestionSectionID = tempData.getInt("TemplateAuditQuestionSectionID");

                    //Add to Data Base
                    vcp = new VendingContentProvider();
                    vcp.insertCompanyAuditQuestion(CompanyAuditQuestionID, CompanyAuditQuestionTitle, CompanyAuditQuestion, QuestionTypeID, QuestionRankID, CompanyAuditID, TemplateAuditQuestionSectionID);

                    //Answer Details
                    JSONArray Answers = tempData.getJSONArray("Answers");

                    for (int j = 0; j < Answers.length(); j++) {
                        JSONObject answerData = Answers.getJSONObject(j);

                        int CompanyAuditAnswerID = answerData.getInt("CompanyAuditAnswerID");
                        String CompanyAuditAnswerGrade = answerData.getString("CompanyAuditAnswerGrade");
                        String CompanyAuditAnswer = answerData.getString("CompanyAuditAnswer");
                        //int CompanyAuditQuestionID = answerData.getInt("CompanyAuditQuestionID");

                        //Add to Data Base
                        vcp = new VendingContentProvider();
                        vcp.insertCompanyAuditAnswer(CompanyAuditAnswerID, CompanyAuditAnswerGrade, CompanyAuditAnswer, CompanyAuditQuestionID);
                    }
                }
            }

            if (GLOBAL_SETTINGS.DEBUG || this.DEBUG)
                Log.d(TAG, "mActivity: " + mActivity.getClass().getSimpleName());
            if (mActivity.getClass().getSimpleName().equals("Login") && startHomeActivity) {
                if (GLOBAL_SETTINGS.DEBUG || this.DEBUG)
                    Log.d(TAG, "mActivity = Login. Finishing Download");
                activityIntent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                activityIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                mActivity.startActivity(activityIntent);
            }
        } catch (Exception e) {
            if (GLOBAL_SETTINGS.DEBUG || this.DEBUG)
                Log.e(TAG, "CompanyAuditAnswers: Error " + e.toString());
        }
    }

    //parses Audit Assets

    void AuditAssetHeaders(String result) {
        JSONArray jArray = null;

        try {
            jArray = new JSONArray(result);
            JSONObject json_data = null;

            //TODO if error .... else

            for (int i = 0; i < jArray.length(); i++) {
                json_data = jArray.getJSONObject(i);

                //Variables parsed from json
                int TemplateAuditID = json_data.getInt("TemplateAuditID");
                String TemplateAuditName = json_data.getString("TemplateAuditName");
                int AuditType = json_data.getInt("AuditAssetTypeID");

                //Inserts into the VCP
                vcp = new VendingContentProvider();
                vcp.insertTemplateAudit(TemplateAuditID, TemplateAuditName, AuditType);
            }
        } catch (Exception e) {
            if (GLOBAL_SETTINGS.DEBUG || this.DEBUG)
                Log.e(TAG, "TemplateAudit: Error " + e.toString());
        }

    }


}
