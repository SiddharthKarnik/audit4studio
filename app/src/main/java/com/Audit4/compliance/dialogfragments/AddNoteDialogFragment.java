package com.Audit4.compliance.dialogfragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.widget.Button;
import android.widget.EditText;

import com.Audit4.compliance.R.id;
import com.Audit4.compliance.R.layout;


@SuppressLint("ValidFragment")
public class AddNoteDialogFragment extends DialogFragment {

	public interface NoticeDialogListener
	{

		void onDialogPositiveClick(AddNoteDialogFragment dialog);
		void onDialogNegativeClick(AddNoteDialogFragment dialog);
		
	}
	
	private String Title;
	private String Positive;
	private String Negative;
	public static String currentText;
	private EditText Notes;

	public AddNoteDialogFragment(){}
	
	public void setAddNoteDialogFragment(String Title, String currentText, String Positive, String Negative)
	{
		this.Title = Title;
		this.currentText = currentText;
		this.Positive = Positive;
		this.Negative = Negative;		
	}
	
	  NoticeDialogListener mListener;

		 @Override
		 public void onAttach(Activity activity) {
			 super.onAttach(activity);
			 // Verify that the host activity implements the callback interface
		     try {
		     	// Instantiate the NoticeDialogListener so we can send events to the host
		    	mListener = (NoticeDialogListener) activity;
		     } catch (ClassCastException e) {
		    	 // The activity doesn't implement the interface, throw exception
		      	 throw new ClassCastException(activity.toString() + " must implement NoticeDialogListener");
		        }
		    }



		@Override
			public Dialog onCreateDialog(Bundle savedInstanceState)
			{
				AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			    LayoutInflater inflater = getActivity().getLayoutInflater();

				builder.setView(inflater.inflate(layout.fragment_add_note, null));
				
				
				
				builder.setTitle(Title)
					.setPositiveButton(Positive, new DialogInterface.OnClickListener()
					{
					public void onClick(DialogInterface dialog, int id)
					{
		                mListener.onDialogPositiveClick(AddNoteDialogFragment.this);
					}
					})
					.setNegativeButton(Negative,  new DialogInterface.OnClickListener()
					{
					public void onClick(DialogInterface dialog, int id)
					{
		                mListener.onDialogNegativeClick(AddNoteDialogFragment.this);
					}
					});					
						
				
				
				return builder.create();
			}

	 @Override
		public void onStart() {
		 // TODO Auto-generated method stub
		 super.onStart();

		 Notes = (EditText) this.getDialog().findViewById(id.notepadEditText);
		 Notes.setText(currentText);
		 Notes.addTextChangedListener(new TextWatcher() {

			 @Override
			 public void beforeTextChanged(CharSequence s, int start,
										   int count, int after) {
				 // TODO Auto-generated method stub

			 }

			 @Override
			 public void onTextChanged(CharSequence s, int start,
									   int before, int count) {
				 // TODO Auto-generated method stub

			 }

			 @Override
			 public void afterTextChanged(Editable s) {
				 currentText = Notes.getText().toString();
			 }
		 });

		 Button positive = ((AlertDialog) getDialog()).getButton(AlertDialog.BUTTON_POSITIVE);
		 positive.setTextColor(Color.parseColor("#027C71"));

		 Button negative = ((AlertDialog) getDialog()).getButton(AlertDialog.BUTTON_NEGATIVE);
		 negative.setTextColor(Color.parseColor("#027C71"));
	 }
}
