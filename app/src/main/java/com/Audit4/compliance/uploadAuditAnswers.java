package com.Audit4.compliance;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.Audit4.compliance.contentprovider.VendingContentProvider;
import com.Audit4.compliance.dialogfragments.GenericDialogFragment;
import com.Audit4.compliance.utils.Util;
import com.Audit4.compliance.utils.Webservice;

public class uploadAuditAnswers extends AsyncTask<String, Integer, String> {

    private VendingContentProvider vcp = new VendingContentProvider();
    private AppCompatActivity mActivity;
    private String idValue = "";

    private boolean DEBUG = true;

    public uploadAuditAnswers(AppCompatActivity activity) {
        this.mActivity = activity;
    }

    //params
    //0 = id
    //1 = result from server
    //2 = TemplateAuditAnswerID
    //3 = CompanyAuditID
    //4 = Additional Notes

    @Override
    protected void onPreExecute() {
        if (mActivity instanceof CompletedAuditListFragment)
            ((CompletedAuditListFragment) mActivity).showProgressBar("Uploading", "Uploading Data, Please Wait");
        else ((Home) mActivity).showProgressBar("Uploading", "Uploading Data, Please Wait");
    }

    @Override
    protected String doInBackground(String... params) {

        if (GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d("UPLOAD", "UPLOADING ANSWERS");
        idValue = params[0];
        return Webservice.uploadAuditAnswers(this.DEBUG, params);
    }

    @Override
    protected void onPostExecute(String result) {
        if (mActivity instanceof CompletedAuditListFragment) {
//            ((CompletedAuditListFragment) mActivity).hideProgressBar();
//            if (result.equals("error")) {
            if (Util.isString(result) && result.contains("false: ")) {
                GenericDialogFragment loginErrorFragment = new GenericDialogFragment();
                loginErrorFragment.setGenericDialogFragment("Audit Upload", "Error Uploading data", "", "Cancel");
                if (!mActivity.isFinishing())
                    loginErrorFragment.show(mActivity.getSupportFragmentManager(), "Audit");
            } else {
                vcp.getAuditImages(idValue, result, mActivity);
                /*if (mActivity instanceof CompletedAuditListFragment)
                    ((CompletedAuditListFragment) mActivity).updateList();*/

            }
        } else {
            ((Home) mActivity).hideProgressBar();
            if (result.equals("error")) {
                GenericDialogFragment loginErrorFragment = new GenericDialogFragment();
                loginErrorFragment.setGenericDialogFragment("Audit", "Error Uploading data", "", "Cancel");
                loginErrorFragment.show(mActivity.getSupportFragmentManager(), "Audit");
            }
        }
    }


}
