package com.Audit4.compliance.login;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import com.Audit4.compliance.GLOBAL_SETTINGS;
import com.Audit4.compliance.utils.Util;
import com.Audit4.compliance.utils.Webservice;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public class RegisterDevice extends AsyncTask<String, Integer, String> {
    private static String TAG = "RegisterDevice";
    private Activity mActivity;

    private boolean DEBUG = false;


    public RegisterDevice(Activity activity) {
        this.mActivity = activity;
    }


    @Override
    protected void onPreExecute() {
        ((Login) mActivity).showProgressBar("Validation", "Please wait while we try register your device");

    }

    @Override
    protected String doInBackground(String... params) {
        if (GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d(TAG, "Registering Device");

        InputStream inputStream = null;
        StringBuilder stringBuilder = null;
        String result = Webservice.obtainLicence(params[0], params[1], params[2]);

        return result;
    }

    @Override
    protected void onPostExecute(String result) {
        if (GLOBAL_SETTINGS.DEBUG || this.DEBUG)
            Log.d(TAG, "Message returned from server: " + result);
        try {
            //jArray = new JSONArray(result);
            JSONObject json_data = new JSONObject(result);

            if (json_data.has("error")) {
                //If licence is not registered
                if (GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d(TAG, "Licence Not Registered!");
                ((Login) mActivity).hideProgressBar();
            } else {
                //If licence is registered

                ((Login) mActivity).hideProgressBar();
                ((Login) mActivity).loginSuccess();
            }
        } catch (Exception e) {
            if (GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.e(TAG, "Error on post " + e.toString());
            ((Login) mActivity).hideProgressBar();
            Util.showErrorInResponse(((Login) mActivity), result);
        }
    }

}
