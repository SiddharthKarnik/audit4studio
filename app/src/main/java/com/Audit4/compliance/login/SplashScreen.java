package com.Audit4.compliance.login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.Audit4.compliance.GLOBAL_SETTINGS;
import com.Audit4.compliance.Home;
import com.Audit4.compliance.R;

public class SplashScreen extends AppCompatActivity {

	final Handler handler = new Handler();
	private Context context;
	private Intent intent;
	
	private boolean DEBUG = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		context = this;
		
		setContentView(R.layout.activity_splashscreen);
		//getActionBar().hide();

		handler.postDelayed(new Runnable() {
			  @Override
			  public void run() {
			    //Do something after 1000ms
				  final boolean DEBUG = false;
				  
				SharedPreferences prefs =  PreferenceManager.getDefaultSharedPreferences(context);
				Boolean LoggedIn = prefs.getBoolean("LoggedIn", false);
				if(GLOBAL_SETTINGS.DEBUG || DEBUG) Log.d("LOG","" + LoggedIn);

				if (LoggedIn == true)
					{intent = new Intent(context, Home.class);
					intent.putExtra("dummy", "dummy");}
				else
					{intent = new Intent(context, Login.class);}
				

				startActivity(intent);
					
			  }
			}, 1000);

	}
}
