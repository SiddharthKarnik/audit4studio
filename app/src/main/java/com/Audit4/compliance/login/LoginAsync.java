package com.Audit4.compliance.login;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.util.Log;

import com.Audit4.compliance.GLOBAL_SETTINGS;
import com.Audit4.compliance.R;
import com.Audit4.compliance.dialogfragments.GenericDialogFragment;
import com.Audit4.compliance.utils.Util;
import com.Audit4.compliance.utils.Webservice;

import org.json.JSONObject;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;


public class LoginAsync extends AsyncTask<String, Integer, String> {
    private static String TAG = "LoginAsync";
    private Activity mActivity;
    private String username;
    //private String deviceID;

    private boolean DEBUG = false;

    public LoginAsync(Activity activity) {
        this.mActivity = activity;
    }

    @Override
    protected String doInBackground(String... params) {

        InputStream inputStream = null;
        StringBuilder stringBuilder = null;
        String result = null;
        result = Webservice.getLogin(params[0], params[1]);
        username = params[0];
        return result;
    }

    @Override
    protected void onPreExecute() {
        ((Login) mActivity).showProgressBar("Login", "Please wait while we verify your details.");
    }

    @Override
    protected void onPostExecute(String result) {
        if (GLOBAL_SETTINGS.DEBUG || this.DEBUG)
            Log.d(TAG, "Message returned from server: " + result);
        try {
            //jArray = new JSONArray(result);
            JSONObject json_data = new JSONObject(result);

            //if (json_data.has("new device"))
            //{
            //	((Login) mActivity).registerDevice(json_data.getInt("new device"));
            //}

            if (json_data.has("error")) {
                //IF an error message is presented from the server
                if (GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d(TAG, "Login Failed!");
                ((Login) mActivity).hideProgressBar();
                ((Login) mActivity).loginFail(json_data.getString("error"));
            } else {
                //IF login is successful - details are added to the shared preferences

                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mActivity.getApplicationContext());
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("Username", username);
                editor.putString("Name", json_data.getString("Name"));
                editor.putString("Email", json_data.getString("Email"));
                editor.putString("CompanyID", json_data.getString("CompanyID"));
                editor.putString("CompanyName", json_data.getString("CompanyName"));
                editor.putString("UserID", json_data.getString("UserID"));
                //editor.putBoolean("LoggedIn", true);

                //editor.putString("deviceID", deviceID);

                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm");
                editor.putString("LastDownLoad", df.format(c.getTime()));
                editor.commit();


                ((Login) mActivity).hideProgressBar();
                ((Login) mActivity).checkDeviceID();

            }
        } catch (Exception e) {
            if (GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.e(TAG, "Error on post " + e.toString());
            if (GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d(TAG, "Unknown Error");
            ((Login) mActivity).hideProgressBar();
            Util.showErrorInResponse(((Login) mActivity), result);

        }
    }
}
