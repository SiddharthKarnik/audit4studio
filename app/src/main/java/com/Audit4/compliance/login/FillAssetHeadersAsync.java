package com.Audit4.compliance.login;

import android.app.Activity;
import android.app.Application;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.Audit4.compliance.GLOBAL_SETTINGS;
import com.Audit4.compliance.contentprovider.VendingContentProvider;
import com.Audit4.compliance.utils.Webservice;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;


public class FillAssetHeadersAsync extends AsyncTask<Void, Integer, String> {
    private static String TAG = "LoginAsync";
    private AppCompatActivity mActivity;

    private boolean DEBUG = false;

    public FillAssetHeadersAsync(AppCompatActivity activity) {
        this.mActivity = activity;
    }

    @Override
    protected String doInBackground(Void... params) {


        String result = Webservice.getAssetHeaders();

        return result;
    }

    @Override
    protected void onPreExecute() {

    }

    @Override
    protected void onPostExecute(String result) {
        if (GLOBAL_SETTINGS.DEBUG || this.DEBUG)
            Log.d(TAG, "Message returned from server: " + result);
        try {

            JSONArray jsonArray = new JSONArray(result);

            if (jsonArray.length() <= 0) {
                Toast.makeText(mActivity, "Error parsing Asset Headers", Toast.LENGTH_SHORT).show();
            } else {
                VendingContentProvider vcp = new VendingContentProvider();
                JSONObject jO = null;
                String [] headers;
                for (int i = 0; i < jsonArray.length(); i++) {
                    jO = (JSONObject) jsonArray.get(i);
                    headers = new String[] {jO.getString("Header1"), jO.getString("Header2"), jO.getString("Header3"), jO.getString("Header4")
                            , jO.getString("Header5")};
                    vcp.addHeadersForAssetType( jO.getInt("AuditAssetTypeID"),
                            headers);
                }

            }
        } catch (Exception e) {
            if (GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.e(TAG, "Error on post " + e.toString());
            if (GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d(TAG, "Unknown Error");
        }
    }
}
