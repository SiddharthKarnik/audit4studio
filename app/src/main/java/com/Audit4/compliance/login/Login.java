package com.Audit4.compliance.login;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings.Secure;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.Audit4.compliance.GLOBAL_SETTINGS;
import com.Audit4.compliance.Home;
import com.Audit4.compliance.JSONReader;
import com.Audit4.compliance.R;
import com.Audit4.compliance.StartAudit;
import com.Audit4.compliance.WebsiteUrl;
import com.Audit4.compliance.dialogfragments.GenericDialogFragment;

public class Login extends AppCompatActivity implements GenericDialogFragment.NoticeDialogListener {
    private static String TAG = "Login";
    private ProgressDialog pd;
    private Button LoginButton;
    private TextView errorMessage, createAccount;
    private EditText username;
    private EditText password;
    private AppCompatActivity mActivity;

    private String dialogType;

    private Bundle bundle;
    private Intent intent;

    private String deviceID, deviceName;

    SharedPreferences prefs;

    private boolean DEBUG = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        LoginButton = (Button) findViewById(R.id.loginSubmit);
        username = (EditText) findViewById(R.id.loginUsername);
        password = (EditText) findViewById(R.id.loginPassword);
        errorMessage = (TextView) findViewById(R.id.loginError);
        createAccount = (TextView) findViewById(R.id.createAccount);
        mActivity = this;

        intent = this.getIntent();
        bundle = this.getIntent().getExtras();

        pd = new ProgressDialog(this);

        if (bundle != null) {
            if (bundle.containsKey("UPDATE_TABLE")) {
                if (bundle.getBoolean("UPDATE_TABLE")) {
                    loginSuccess(); //called when activity started from CheckUpdate Request
                    LoginButton.setVisibility(View.INVISIBLE);
                    username.setVisibility(View.INVISIBLE);
                    password.setVisibility(View.INVISIBLE);
                    showProgressBar("Checking For Updates", "Downloading Data");
                }
            }
        }


        prefs = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());

        username.setText(prefs.getString("Username", ""));

        deviceID = prefs.getString("deviceID", "");

        if (deviceID == "") {
            deviceID = newDevice();
        }

        LoginButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final boolean DEBUG = false;
                if (GLOBAL_SETTINGS.DEBUG || DEBUG) Log.d(TAG, "Login button clicked");
                if (GLOBAL_SETTINGS.networkIsOnline(mActivity)) {
                    new LoginAsync(mActivity).execute(username.getText().toString(), password.getText().toString());

                } else {
                    dialogType = "CONNECTION";
                    GenericDialogFragment newFragment = new GenericDialogFragment();
                    newFragment.setGenericDialogFragment("Login", "You don't have an active data connection, please check your connection and try again!", "Open Network Settings", "Cancel");
                    newFragment.show(getSupportFragmentManager(), "login");
                }
            }
        });

        createAccount.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Uri uriUrl = Uri.parse(WebsiteUrl.createAccount);
                Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
                startActivity(launchBrowser);
            }
        });

    }

    void checkDeviceID() {
        if (GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d(TAG, "checkDeviceID");
        new LicenceCheck(mActivity).execute(deviceID, prefs.getString("CompanyID", "INVALID"));
    }

    void loginSuccess() {
        new FillAssetHeadersAsync(Login.this).execute();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mActivity.getApplicationContext());
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("LoggedIn", true);
        editor.commit();

        if (GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d(TAG, "Login sucess!");
        if (intent.hasExtra("MachineID")) {
            if (GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d("TAG", "KEY");
            Intent intent = new Intent(this, StartAudit.class);
            intent.putExtra("MachineID", bundle.getString("MachineID"));
            JSONReader.Populate(mActivity, intent);
        } else {
            Intent intent = new Intent(this, Home.class);
            JSONReader.Populate(mActivity, intent);
            if (GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d("TAG", "NOKEY");

        }
    }

    void loginFail(String error) {
        //errorMessage.setText(error);
        dialogType = "LOGIN";
        GenericDialogFragment loginErrorFragment = new GenericDialogFragment();
        loginErrorFragment.setGenericDialogFragment("Login", "Login Failed! Please check your credentials and try again.", "Retry", "Cancel");
        loginErrorFragment.show(getSupportFragmentManager(), "login");
        //open dialogfragment to show error

    }

    public void showProgressBar(String title, String message) {
        pd.setCancelable(false);
        pd.setTitle(title);
        pd.setMessage(message);
        pd.show();
    }

    public void hideProgressBar() {
        pd.hide();
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        if (dialogType.equals("CONNECTION")) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.setClassName("com.android.phone", "com.android.phone.NetworkSetting");
            startActivity(intent);
        } else if (dialogType.equals("LOGIN")) {
            new LoginAsync(mActivity).execute(username.getText().toString(), password.getText().toString());
        } else if (dialogType.equals("REGISTER")) {
            inputDeviceName();
        } else if (dialogType.equals("UNABLE")) {

        } else if (dialogType.equals("noAuditsAssigned")) {

        }
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
        //dismiss
    }

    String newDevice() {
        //SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmssSS");
        //String data = sdf.format(new Date());
        //String s = new String(Hex.encodeHex(DigestUtils.md5(data)));

        String s = Secure.getString(this.getContentResolver(), Secure.ANDROID_ID);
        return s;
    }

    public void registerDevice(int remaining) {
        dialogType = "REGISTER";
        GenericDialogFragment loginErrorFragment = new GenericDialogFragment();
        loginErrorFragment.setGenericDialogFragment("Login", "This device has not been registered. You can register " + remaining + " more device(s). Register Device?", "Register", "Cancel");
        loginErrorFragment.show(getSupportFragmentManager(), "register");
    }

    public void unableToRegister() {
        dialogType = "UNABLE";
        GenericDialogFragment loginErrorFragment = new GenericDialogFragment();
        loginErrorFragment.setGenericDialogFragment("Login", "Your company has no remaining devices to register. Please contact Audit4 to order more", "Continue", "Cancel");
        loginErrorFragment.show(getSupportFragmentManager(), "unable");
    }

    void inputDeviceName() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle("Enter a name for this Device");

        // Set an EditText view to get user input
        final EditText input = new EditText(this);
        alert.setView(input);

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                deviceName = input.getText().toString();
                new RegisterDevice(mActivity).execute(deviceID, prefs.getString("CompanyID", "INVALID"), deviceName);
            }
        });
        AlertDialog alertDialog = alert.show();
        Button positive = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
        positive.setTextColor(Color.parseColor("#027C71"));
        Button negative = alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE);
        negative.setTextColor(Color.parseColor("#027C71"));
    }

    public void noAuditsAssigned() {
        dialogType = "noAuditsAssigned";
        GenericDialogFragment newFragment = new GenericDialogFragment();
        newFragment.setGenericDialogFragment("Login", "No Audits have been assigned to this account. Please contact your company consultant", "Continue", "");
        newFragment.show(getSupportFragmentManager(), "login");
        clearPrefs();
    }

    void clearPrefs() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("LoggedIn", false);
        editor.putString("Name", "");
        editor.putString("Email", "");
        editor.putString("CompanyID", "");
        editor.putString("CompanyName", "");
        editor.putString("UserID", "");
        editor.commit();
    }

    public void newDialog(String Title, String Message, String PostivieAction, String NegativeAction, String type) {
        GenericDialogFragment newFragment = new GenericDialogFragment();
        newFragment.setGenericDialogFragment(Title, Message, PostivieAction, NegativeAction);
        newFragment.show(getSupportFragmentManager(), type);
        dialogType = type;
    }
}
