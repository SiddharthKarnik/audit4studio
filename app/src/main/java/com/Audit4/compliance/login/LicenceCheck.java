package com.Audit4.compliance.login;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import com.Audit4.compliance.GLOBAL_SETTINGS;
import com.Audit4.compliance.utils.Util;
import com.Audit4.compliance.utils.Webservice;

import org.json.JSONObject;

import java.io.InputStream;

public class LicenceCheck extends AsyncTask<String, Integer, String> {
    private static String TAG = "LicenceCheck";
    private Activity mActivity;


    private boolean DEBUG = false;


    public LicenceCheck(Activity activity) {
        this.mActivity = activity;
    }

    @Override
    protected void onPreExecute() {
        ((Login) mActivity).showProgressBar("Validation", "Please wait while we verify your Licence.");

    }

    @Override
    protected String doInBackground(String... params) {

        if (GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d(TAG, "Checking Licence");

        InputStream inputStream = null;
        StringBuilder stringBuilder = null;
        String result = null;
        result = Webservice.licenceCheck(params[0], params[1]);

        return result;
    }

    @Override
    protected void onPostExecute(String result) {
        if (GLOBAL_SETTINGS.DEBUG || this.DEBUG)
            Log.d(TAG, "Message returned from server: " + result);
        try {
            //jArray = new JSONArray(result);
            JSONObject json_data = new JSONObject(result);

            if (json_data.has("error")) {
                //If licence is not registered
                if (GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d(TAG, "Licence Not Registered!");
                ((Login) mActivity).hideProgressBar();

                if (GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d(TAG, "" + json_data.get("error"));

/* invalid license*/
                if (json_data.get("error").equals("invalid licence")) {
                    if (GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d(TAG, "HERE");

                    int available = 0;
                    if (json_data.has("licences")) {
                        available = json_data.getInt("licences");
                    }

                    if (available > 0) {
                        ((Login) mActivity).registerDevice(available);
                    } else {
                        ((Login) mActivity).unableToRegister();
                    }
                }
/*out of date*/
                else if (json_data.get("error").equals("licence out of date")) {
                    //License is out of date - they can still log in but will be unable to upload
                    ((Login) mActivity).showProgressBar("Login Sucessful", "Downloading Data");
                    ((Login) mActivity).loginSuccess();
                } else {
                    if (GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d(TAG, "CHECK FAILED");
/*Show message for failure*/
                    ((Login) mActivity).newDialog("Invalid Licence", result, "Continue", "", "invalid");
                }
            } else {
                //If licence is registered
                ((Login) mActivity).showProgressBar("Login Sucessful", "Downloading Data");
                ((Login) mActivity).loginSuccess();
            }
        } catch (Exception e) {
            if (GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.e(TAG, "Error on post " + e.toString());
            ((Login) mActivity).hideProgressBar();
            Util.showErrorInResponse(((Login) mActivity), result);
        }
    }

}
