package com.Audit4.compliance;

import android.app.ActivityOptions;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.Audit4.compliance.audit.Audit;
import com.Audit4.compliance.contentprovider.VendingContentProvider;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class StartAudit extends PersonalDetailsInfoBar {
    private Bundle bundle;
    private VendingContentProvider vcp = new VendingContentProvider();

    private EditText tRouteID, tMachineID, tTemplate, tClient, tClientSite;
    private LinearLayout llRouteID, llMachineID, llTemplate, llClient, llClientSite;
    private TextView sectionA, sectionB, sectionC, sectionD;

    private int AuditType = 0;

    private boolean DEBUG = false;

    public static final String MIME_TEXT_PLAIN = "text/plain";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_audit);

        //If Bundle data available this will auto-fill in the details below
        tRouteID = (EditText) findViewById(R.id.startRouteID);
        tMachineID = (EditText) findViewById(R.id.startMachineID);
        tTemplate = (EditText) findViewById(R.id.Template);
        EditText tDate = (EditText) findViewById(R.id.startDate);
        tClient = (EditText) findViewById(R.id.Client);
        tClientSite = (EditText) findViewById(R.id.ClientSite);
        llTemplate = (LinearLayout) findViewById(R.id.llTemplate);
        llRouteID = (LinearLayout) findViewById(R.id.llstartRouteID);
        llMachineID = (LinearLayout) findViewById(R.id.llstartMachineID);
        llClient = (LinearLayout) findViewById(R.id.llClient);
        llClientSite = (LinearLayout) findViewById(R.id.llClientSite);
        bundle = getIntent().getExtras();

        setAuditType();

        //tRouteID.setText(getIntent().getStringExtra("RouteName"));
        //tMachineID.setText(getIntent().getStringExtra("MachineID"));
        tTemplate.setText(getIntent().getStringExtra("Template"));

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        tDate.setText(df.format(c.getTime()));


        setUpTemplate();


        //Buttons
        Button startAudit = (Button) findViewById(R.id.startStart);

        startAudit.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {

                //if (AuditType == 1 || AuditType == 5) //Contains RouteField
                //{
                if (tRouteID.getText().length() != 0 && tMachineID.getText().length() != 0 && tClient.getText().length() != 0 && tClientSite.getText().length() != 0) {
                    startAudit();
                }
                //}
                //else
                //{
                //	tRouteID.setText("N/A.");
                //	if (tMachineID.getText().length() != 0 && tClient.getText().length() != 0 && tClientSite.getText().length() != 0)
                //	{startAudit();}
                //}
            }
        });
    }

    void startAudit() {
        Intent intent = new Intent(getBaseContext(), Audit.class);

        intent.putExtra("RouteID", tRouteID.getText().toString());
        intent.putExtra("MachineID", tMachineID.getText().toString());
        intent.putExtra("ClientPostcode", bundle.getString("ClientPostcode", "Unknown"));
        intent.putExtra("Template", getIntent().getStringExtra("Template"));
        intent.putExtra("TemplateID", AuditType);
        intent.putExtra("AuditID", getIntent().getIntExtra("AuditID", -1));
        intent.putExtra("Client", tClient.getText().toString());
        intent.putExtra("ClientSite", tClientSite.getText().toString());

        ActivityOptions opts = ActivityOptions.makeCustomAnimation(getBaseContext(),
                R.anim.fade_in, R.anim.fade_out);
        startActivity(intent, opts.toBundle());
    }

    void setUpTemplate() {
        Audit.questionList.clear();

        int auditID = getIntent().getIntExtra("AuditID", -1);
        int templateAuditID = vcp.getTemplateAuditID(auditID);
        if (GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d("TemplateAuditID", " " + templateAuditID);
        int totalSections = vcp.getTotalSections(templateAuditID);
        if (GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d("totalSections", " " + totalSections);
        String templateName = getIntent().getStringExtra("Template");


        for (int i = 1; i < totalSections + 1; i++) {
            int position = vcp.getTemplatePosition(templateAuditID, i);
            vcp.getTemplateQuestions(templateAuditID, position); //last value = TemplateAuditQuestionSectionID
            vcp.getCompanyAuditQuestions(templateAuditID, position, templateName);
        }
    }

    void setAuditType() //NEEDS REPEATING IN SUMMARY
    {
        AuditType = vcp.getTemplateType(getIntent().getIntExtra("TemplateID", 0));

        sectionA = (TextView) findViewById(R.id.sectionA);
        sectionB = (TextView) findViewById(R.id.sectionB);
        sectionC = (TextView) findViewById(R.id.sectionC);
        sectionD = (TextView) findViewById(R.id.sectionD);

        Log.d("startaudit", "AuditType: " + AuditType);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());


        String[] headers = vcp.getHeadersForAssetType(AuditType);

        if (!headers[0].isEmpty()) {
            sectionB.setText(headers[0] + ":");
        } else {
            sectionB.setVisibility(View.GONE);
            sectionB.setText("N/A:");
            tMachineID.setVisibility(View.GONE);
            tMachineID.setText("N/A");
            llMachineID.setVisibility(View.GONE);
        }

        if (!headers[1].isEmpty()) {
            sectionC.setText(headers[1] + ":");
        } else {
            sectionC.setVisibility(View.GONE);
            sectionC.setText("N/A:");
            tClient.setVisibility(View.GONE);
            tClient.setText("N/A");
            llClient.setVisibility(View.GONE);
        }

        if (!headers[2].isEmpty()) {
            sectionD.setText(headers[2] + ":");
        } else {
            sectionD.setVisibility(View.GONE);
            sectionD.setText("N/A:");
            tClientSite.setVisibility(View.GONE);
            tClientSite.setText("N/A");
            llClientSite.setVisibility(View.GONE);
        }

        if (!headers[3].isEmpty()) {
            sectionA.setText(headers[3]+":");
        } else {
            sectionA.setVisibility(View.GONE);
            sectionA.setText("N/A:");
            tRouteID.setVisibility(View.GONE);
            tRouteID.setText("N/A");
            llRouteID.setVisibility(View.GONE);
        }

		/*switch(AuditType)
        {
		case 1: //Vending
			sectionA.setText("Route No:");
			sectionB.setText("Asset No:");
			sectionC.setText("Client:");
			sectionD.setText("Client Site:");
			break;
		case 2: //Function
			sectionA.setVisibility(View.GONE);
			tRouteID.setVisibility(View.GONE);
			sectionA.setText("N/A:");
			tRouteID.setText("N/A");
			
			sectionB.setText("Reference Number:");
			sectionC.setText("Auditee:");
			sectionD.setText("Location:");			
			break;
		case 3: //Clause Audit
			sectionA.setVisibility(View.GONE);
			tRouteID.setVisibility(View.GONE);
			sectionA.setText("N/A:");
			tRouteID.setText("N/A");

			sectionB.setText("Reference Number:");
			sectionC.setText("Auditee:");
			sectionD.setText("Location:");				
			break;
		case 4: //Staff Assessment
			sectionA.setVisibility(View.GONE);
			tRouteID.setVisibility(View.GONE);
			sectionA.setText("N/A:");
			tRouteID.setText("N/A");
			
			sectionB.setText("Staff ID:");
			sectionC.setText("Name:");
			sectionD.setText("Team/Department:");		
			break;
		case 5: //Vehicle
			sectionA.setVisibility(View.GONE);
			tRouteID.setVisibility(View.GONE);
			sectionA.setText("N/A:");
			tRouteID.setText("N/A");

			sectionB.setText("Registration Number:");
			sectionC.setText("Driver:");
			sectionD.setText("Location:");	
			break;
		
		case 6: //Health and Safety
			sectionA.setVisibility(View.GONE);
			tRouteID.setVisibility(View.GONE);
			sectionA.setText("N/A:");
			tRouteID.setText("N/A");

			sectionB.setText("Reference Number:");
			sectionC.setText("Auditee:");
			sectionD.setText("Location:");		
			break;
		}*/

    }
}
