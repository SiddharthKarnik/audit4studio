package com.Audit4.compliance;

public class CompletedAuditAnswer {
	private int completedAuditID;
	private int auditTemplateQuestionID;
	private int auditCompanyQuestionID;
	private int answerID;
	private String additionalNotes;
	
	public CompletedAuditAnswer(int completedAuditID, int auditTemplateQuestionID, int auditCompanyQuestionID, int answerID, String additionalNotes){
		this.completedAuditID = completedAuditID;
		this.auditTemplateQuestionID = auditTemplateQuestionID;
		this.auditCompanyQuestionID = auditCompanyQuestionID;
		this.answerID = answerID;
		this.additionalNotes = additionalNotes;
	}
	
	public int getCompletedAuditID() {
		return completedAuditID;
	}
	
	public int getAuditTemplateQuestionID() {
		return auditTemplateQuestionID;
	}
	
	public int auditCompanyQuestionID() {
		return auditCompanyQuestionID;
	}
	
	public int getAnswerID() {
		return answerID;
	}
	
	public String getAdditionalNotes() {
		return additionalNotes;
	}
}
