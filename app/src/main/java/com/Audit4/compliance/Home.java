package com.Audit4.compliance;

import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.Audit4.compliance.contentprovider.VendingContentProvider;
import com.Audit4.compliance.dialogfragments.GenericDialogFragment;
import com.Audit4.compliance.login.Login;
import com.Audit4.compliance.templates.TemplateList;
import com.Audit4.compliance.utils.Util;

public class Home extends PersonalDetailsInfoBar implements GenericDialogFragment.NoticeDialogListener{
	
	private boolean DEBUG = false;
	
	private ProgressDialog pd;
	private static String dialogType;
	private String TAG = "HOME.JAVA";
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if(GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d(TAG,"On create");
		if(vcp==null) vcp = new VendingContentProvider();
		setContentView(R.layout.activity_home);
		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
		pd = new ProgressDialog(this);
	}
	

	
	@Override
	protected void onNewIntent(Intent intent) {
		// TODO Auto-generated method stub
		super.onNewIntent(intent);
		if(GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d(TAG,"On NEW INTENT");
		hideProgressBar();
	}



	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		hideProgressBar();
	}



	public void showProgressBar(String title, String message) {
		pd.setCancelable(false);
		pd.setTitle(title);
		pd.setMessage(message);
		pd.show();
	}

	public void hideProgressBar() {
		pd.hide();
	}
	
	
	public void swapUploadFragment() {
		Intent i = new Intent(this, CompletedAuditListFragment.class);
		startActivity(i);
	}
	
	
	
	public static class PlaceholderFragment extends Fragment {
		private VendingContentProvider vcp;
		private RelativeLayout startAudit;
		private RelativeLayout updateAudit;
		private static RelativeLayout uploadAudits;

		
		public PlaceholderFragment() {
		}
		
		@Override
		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			//vcp = new VendingContentProvider();
			Home parent = (Home)this.getActivity();
			vcp = parent.vcp;
			
			
			/*vcp.addHeadersForAssetType(1, new String[]{"1","2","3","4","5"});
			vcp.addHeadersForAssetType(2, new String[]{"1","2","3","4","5"});
			vcp.addHeadersForAssetType(3, new String[]{"1","2","3","4","5"});
			vcp.addHeadersForAssetType(4, new String[]{"1","2","3","4","5"});
			vcp.addHeadersForAssetType(5, new String[]{"1","2","3","4","5"});
			vcp.addHeadersForAssetType(6, new String[]{"1","2","3","4","5"});
			vcp.addHeadersForAssetType(7, new String[]{"1","2","3","4","5"});*/
			
		}
		
		@Override
		public void onResume() {
			super.onResume();
			setUploadButton();
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_home, container, false);
			
			startAudit = (RelativeLayout) rootView.findViewById(R.id.rl_start_audit);
			updateAudit = (RelativeLayout) rootView.findViewById(R.id.rl_check_updates);
			uploadAudits = (RelativeLayout) rootView.findViewById(R.id.rl_completed_audits);

			setUploadButton();
			
			SharedPreferences prefs =  PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());


			//Start
			startAudit.setOnClickListener(new OnClickListener() {
	            public void onClick(View v) {
	            	Intent intent = new Intent (getActivity().getBaseContext(), TemplateList.class);
	            	ActivityOptions opts=ActivityOptions.makeCustomAnimation(getActivity().getBaseContext(), 
							R.anim.fade_in, R.anim.fade_out);
	            	startActivity(intent,opts.toBundle());
	            }
	        });

			uploadAudits.setOnClickListener(new OnClickListener() {
	            public void onClick(View v) {
	            	//startUploads();
	            	Home act = (Home)getActivity();
	            	act.swapUploadFragment();
	            }
	        });
			
			updateAudit.setOnClickListener(new OnClickListener() {
				public void onClick(View v){

					if(Util.isInternetConnected((Home)getActivity())) {
						checkUpdates();
					}else{
						Util.showNoInternetAlert((Home)getActivity());
					}
				}
			});	
			
			return rootView;
		}
		
		

		public void startUploads() {
			new uploadLicenceCheck(this.getActivity()).execute();	
		}
		
		public void setUploadButton() {
			if(vcp.getAuditsToUpload()) uploadAudits.setEnabled(true);
			else 
			uploadAudits.setEnabled(true);
		}
	
		public void checkUpdates()
		{
			GenericDialogFragment newFragment = new GenericDialogFragment();
			newFragment.setGenericDialogFragment("Update", "This will refresh the audit templates with the latest version. Are you sure you wish to continue?", "Continue", "Cancel");
			newFragment.show(getActivity().getSupportFragmentManager(), "update");
			dialogType = "updates";
		}
	}
	
	public void newDialog(String Title, String Message, String PostivieAction, String NegativeAction, String type)
	{
		GenericDialogFragment newFragment = new GenericDialogFragment();
		newFragment.setGenericDialogFragment(Title, Message, PostivieAction, NegativeAction);
		newFragment.show(getSupportFragmentManager(), type);
		dialogType = type;
	}

	@Override
	public void onDialogPositiveClick(DialogFragment dialog) {
		
		switch(dialogType) {
		case "updates":
			dialogType = "";
			vcp.clearTables();
			
			Logout();
			break;
		
		}
		dialog.dismiss();
		
	}

	@Override
	public void onDialogNegativeClick(DialogFragment dialog) {
		
		// TODO Auto-generated method stub
		
	}
	
	public void Logout()
	{	
    	Intent intent = new Intent(this, Login.class);
    	intent.putExtra("UPDATE_TABLE", true);
    	intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP| Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(intent);
	}
	
	public void uploadData()
	{
		vcp.getCompletedAudits(this.getApplicationContext(),this);
		PlaceholderFragment.uploadAudits.setEnabled(false);
	}

	
}
