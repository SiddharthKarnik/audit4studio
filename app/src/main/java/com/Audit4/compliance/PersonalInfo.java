package com.Audit4.compliance;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class PersonalInfo extends Fragment {
	
	TextView username;
	TextView email;
	TextView company, lastDownload;
	
	private boolean DEBUG = false;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_personal_info, container, false);
		
		
		username = (TextView) rootView.findViewById(R.id.section);
		email = (TextView) rootView.findViewById(R.id.questionNumber);
		company = (TextView) rootView.findViewById(R.id.question);
		lastDownload = (TextView) rootView.findViewById(R.id.lastDownload);
		
		SharedPreferences prefs =  PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
		username.setText(prefs.getString("Name","Missing"));
		email.setText(prefs.getString("Email","Missing"));
		company.setText(prefs.getString("CompanyName","Missing"));
		lastDownload.setText("Last Check for Updates: " + prefs.getString("LastDownLoad", "Missing") );

		
		return rootView;
	}

	@Override
	public void onStart() {
		super.onStart();
		SharedPreferences prefs =  PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
		lastDownload.setText("Last Check for Updates: " + prefs.getString("LastDownLoad", "Missing") );
	}
}
