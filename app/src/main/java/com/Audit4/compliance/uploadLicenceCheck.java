package com.Audit4.compliance;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.provider.Settings;

import com.Audit4.compliance.utils.Webservice;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public class uploadLicenceCheck extends AsyncTask<String, Integer, String> {
    private Activity mActivity;
    /*	HttpClient httpclient;
        HttpPost httppost;
        */
    SharedPreferences prefs;


    public uploadLicenceCheck(Activity activity) {
        this.mActivity = activity;
    }

    @Override
    protected void onPreExecute() {
        ((Home) mActivity).showProgressBar("Validation", "Please wait while we verify your Licence");

	/*	httpclient = new DefaultHttpClient();
		httppost = new HttpPost(WebsiteUrl.webURl +"/includes/APIS/checkLicence.php");*/
        prefs = PreferenceManager.getDefaultSharedPreferences(mActivity.getApplicationContext());
    }

    @Override
    protected String doInBackground(String... params) {

        InputStream inputStream = null;
        StringBuilder stringBuilder = null;
        String result = null;

        String deviceID = Settings.Secure.getString(mActivity.getContentResolver(), Settings.Secure.ANDROID_ID);
        String companyID = prefs.getString("CompanyID", "INVALID");
        Webservice.licenceCheck(deviceID, companyID);

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            stringBuilder = new StringBuilder();
            stringBuilder.append(reader.readLine() + "\n");

            String line = "0";
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }
            inputStream.close();
            result = stringBuilder.toString();

        } catch (Exception e) {
        }
        return result;
    }

    @Override
    protected void onPostExecute(String result) {
        try {
            //jArray = new JSONArray(result);
            JSONObject json_data = new JSONObject(result);

            if (json_data.has("error")) {
                if (json_data.get("error").equals("licence out of date")) {
                    ((Home) mActivity).newDialog("Error", "Cannot upload at this time - Your licence has expired", "Continue", "", "uploadError");
                } else {
                    ((Home) mActivity).newDialog("Error", "Cannot upload at this time - Your licence is invalid", "Continue", "", "uploadError");
                }
            } else {
                ((Home) mActivity).uploadData();
            }
        } catch (Exception e) {
        }finally {
            ((Home) mActivity).hideProgressBar();
        }
    }
}
