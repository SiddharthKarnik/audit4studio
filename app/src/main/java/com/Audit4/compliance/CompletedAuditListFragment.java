package com.Audit4.compliance;

import android.app.ListFragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings.Secure;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;

import com.Audit4.compliance.contentprovider.VendingContentProvider;
import com.Audit4.compliance.dialogfragments.GenericDialogFragment;
import com.Audit4.compliance.login.Login;
import com.Audit4.compliance.utils.Util;
import com.Audit4.compliance.utils.Webservice;

import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;


public class CompletedAuditListFragment extends AppCompatActivity
        implements GenericDialogFragment.NoticeDialogListener {

    private Button uploadAudits, oldAudits;
    private ProgressDialog pd;
    private static String dialogType;
    private static VendingContentProvider vcp = new VendingContentProvider();
    private PlaceholderFragment ph;

    public int serverID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_completedauditfragmentlist);

        ph = new PlaceholderFragment();

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, ph).commit();
        }

        uploadAudits = (Button) findViewById(R.id.upload_audit_btn);

        uploadAudits.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {


                if (Util.isInternetConnected(CompletedAuditListFragment.this)) {
                    uploadAudits.setEnabled(false);
                    startUploads();

                } else {
                    Util.showNoInternetAlert(CompletedAuditListFragment.this);

                }
            }
        });

        pd = new ProgressDialog(this);

        oldAudits = (Button) findViewById(R.id.OldAudits);

        oldAudits = (Button) findViewById(R.id.OldAudits);
        oldAudits.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                //newDialog("WARNING","This action will REMOVE all audits on the device. Are you sure?","Confirm","Cancel","","DeleteAll");
                updateOldList();
            }
        });

        //deleteAudits.getBackground().setColorFilter(R.color.red, PorterDuff.Mode.XOR);
        //oldAudits.setBackgroundColor(getResources().getColor(R.color.red));
    }

    @Override
    public void onResume() {
        super.onResume();
        if (vcp.getAuditsToUpload()) {
            uploadAudits.setEnabled(true);
            uploadAudits.setVisibility(View.VISIBLE);
        } else {
            uploadAudits.setEnabled(false);
            uploadAudits.setVisibility(View.INVISIBLE);
            LinearLayout main = (LinearLayout) findViewById(R.id.completedauditlist_main);
            main.removeView(uploadAudits);

        }

    }

    public void showProgressBar(String title, String message) {

        pd.setCancelable(false);
        pd.setTitle(title);
        pd.setMessage(message);
        pd.show();
    }

    public void hideProgressBar() {
        pd.hide();
    }

    public void startUploads() {
        new uploadAuditsToWebsite(this).execute();
    }

    public void newDialog(String Title, String Message, String PostivieAction, String NegativeAction) {
        GenericDialogFragment newFragment = new GenericDialogFragment();
        newFragment.setGenericDialogFragment(Title, Message, PostivieAction, NegativeAction);
        newFragment.show(getSupportFragmentManager(), "tag"/*type*/);
        //dialogType = type;
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {

		/*switch(dialogType) {
        case "updates":
			dialogType = "";
			vcp.clearTables();
			
			Logout();
			break;
		case "Reupload":
			vcp.reuploadAudit(this, this, serverID);
			break;
		case "OldAudit":
			break;
		case "DeleteAll":
			vcp.DeleteAllByUserID(this.getApplicationContext());
          	finish();
           	startActivity(getIntent());
			break;
		
		}*/
        if (Util.isInternetConnected(CompletedAuditListFragment.this)) {
            vcp.reuploadAudit(this, this, serverID);
        } else {
            Util.showNoInternetAlert(CompletedAuditListFragment.this);
        }

        dialog.dismiss();

    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {

    }


    public void Logout() {
        Intent intent = new Intent(this, Login.class);
        intent.putExtra("UPDATE_TABLE", true);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    public void uploadData() {
        vcp.getCompletedAudits(this.getApplicationContext(), this);
    }

    public void updateList() {
        ph.listItems.clear();
        ph.listItems = vcp.getCompletedAuditsForListFragment(this.getApplicationContext(), this, ph.listItems);
        ph.myAuditRowAdapter.notifyDataSetChanged();

        oldAudits.setText("Goto Old Audits");
        oldAudits.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                //newDialog("WARNING","This action will REMOVE all audits on the device. Are you sure?","Confirm","Cancel","","DeleteAll");
                updateOldList();
            }
        });

        if (vcp.getAuditsToUpload()) {

            //uploadAudits.setEnabled(true);
            uploadAudits.setVisibility(View.VISIBLE);
        } else {
            //uploadAudits.setEnabled(false);
            uploadAudits.setVisibility(View.INVISIBLE);
            LinearLayout main = (LinearLayout) findViewById(R.id.completedauditlist_main);
            main.removeView(uploadAudits);

        }
    }

    public void updateOldList() {
        ph.listItems.clear();
        ph.listItems = vcp.getOldAuditsForListFragment(this.getApplicationContext(), this, ph.listItems);
        ph.myAuditRowAdapter.notifyDataSetChanged();

        oldAudits.setText("Goto New Audits");
        oldAudits.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                //newDialog("WARNING","This action will REMOVE all audits on the device. Are you sure?","Confirm","Cancel","","DeleteAll");
                updateList();
            }
        });
        uploadAudits.setVisibility(View.INVISIBLE);
        LinearLayout main = (LinearLayout) findViewById(R.id.completedauditlist_main);
        main.removeView(uploadAudits);
    }

    public static class PlaceholderFragment extends ListFragment {

        private ArrayList<AuditItem> listItems = new ArrayList<AuditItem>();
        public MyAuditRowAdapter myAuditRowAdapter;


        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            listItems = vcp.getCompletedAuditsForListFragment(this.getActivity().getApplicationContext(), this.getActivity(), listItems);
            myAuditRowAdapter = new MyAuditRowAdapter(getActivity(),
                    R.layout.audit_row, listItems);

            setListAdapter(myAuditRowAdapter);
            setHasOptionsMenu(true);

            setRetainInstance(true);


        }


        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            // TODO Auto-generated method stub
            super.onActivityCreated(savedInstanceState);
            getListView().setDivider(new ColorDrawable(Color.TRANSPARENT));
            getListView().setDividerHeight(1);
        }
    }

    private class uploadAuditsToWebsite extends AsyncTask<String, Integer, String> {
        private CompletedAuditListFragment mActivity;

        SharedPreferences prefs;


        public uploadAuditsToWebsite(CompletedAuditListFragment activity) {
            this.mActivity = activity;
        }

        @Override
        protected void onPreExecute() {
            mActivity.showProgressBar("Validation", "Please wait while we verify your Licence");
            prefs = PreferenceManager.getDefaultSharedPreferences(mActivity.getApplicationContext());
        }

        @Override
        protected String doInBackground(String... params) {

            InputStream inputStream = null;
            StringBuilder stringBuilder = null;
            String result = null;

            String deviceID = Secure.getString(mActivity.getContentResolver(), Secure.ANDROID_ID);
            String companyID = prefs.getString("CompanyID", "INVALID");
            result = Webservice.licenceCheck(deviceID, companyID);

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                //jArray = new JSONArray(result);
                JSONObject json_data = new JSONObject(result);

                if (json_data.has("error")) {
                    mActivity.hideProgressBar();
                    uploadAudits.setEnabled(true);
                    if (json_data.get("error").equals("licence out of date")) {
                        mActivity.newDialog("Error", "Cannot upload at this time - Your licence has expired", "Continue", "");
                    } else {
                        mActivity.newDialog("Error", "Cannot upload at this time - Your licence is invalid", "Continue", "");
                    }
                } else {
                    mActivity.hideProgressBar();
                    uploadAudits.setEnabled(true);
                    mActivity.uploadData();
                }
            } catch (Exception e) {

            }/*finally {
                mActivity.hideProgressBar();
                uploadAudits.setEnabled(true);
            }*/
        }

    }


}
