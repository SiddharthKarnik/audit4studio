package com.Audit4.compliance;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.Audit4.compliance.contentprovider.VendingContentProvider;
import com.Audit4.compliance.dialogfragments.GenericDialogFragment;
import com.Audit4.compliance.login.Login;

public class Account extends PersonalDetailsInfoBar implements GenericDialogFragment.NoticeDialogListener {

    private boolean DEBUG = false;

    private VendingContentProvider vcp;

    private Button cleardata;
    private Button checkUpdates;
    private ProgressDialog pd;
    private String dialogType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);

        pd = new ProgressDialog(this);

        cleardata = (Button) findViewById(R.id.routetextlayout);
        cleardata.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                dialogType = "DELETEDB";
                GenericDialogFragment newFragment = new GenericDialogFragment();
                newFragment.setGenericDialogFragment("Warning", "This will log you out and delete all local data. Do you wish to continue?", "Continue", "Cancel");
                newFragment.show(getSupportFragmentManager(), "clearData");
            }
        });


        checkUpdates = (Button) findViewById(R.id.Account_checkforupdates);
        checkUpdates.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (GLOBAL_SETTINGS.networkIsOnline(Account.this)) {
                    Intent intent = new Intent(Account.this, Account.class);
                    JSONReader.Populate(Account.this, intent);
                } else {
                    dialogType = "CONNECTION";
                    GenericDialogFragment newFragment = new GenericDialogFragment();
                    newFragment.setGenericDialogFragment("Account", "You don't have an active data connection, please check your connection and try again!", "Open Network Settings", "Cancel");
                    newFragment.show(getSupportFragmentManager(), "accountConection");
                }
            }
        });

    }

    public void clearLocalData() {
        vcp = new VendingContentProvider();
        vcp.clearTables();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
        prefs.edit().remove("Username");

        prefs.edit().commit();

        Intent intent = new Intent(this, Login.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }


    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        if (dialogType.equals("DELETEDB")) {
            if (GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d("tag", "Depricated");
            //clearLocalData();

        }

        if (dialogType.equals("CONNECTION")) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.setClassName("com.android.phone", "com.android.phone.NetworkSetting");
            startActivity(intent);
        }
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
    }

    public void showProgressBar(String title, String message) {
        pd.setCancelable(false);
        pd.setTitle(title);
        pd.setMessage(message);
        pd.show();
    }

    public void hideProgressBar() {
        pd.hide();
    }
}
