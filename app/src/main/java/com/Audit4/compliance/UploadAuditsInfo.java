package com.Audit4.compliance;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.Audit4.compliance.contentprovider.VendingContentProvider;
import com.Audit4.compliance.login.Login;
import com.Audit4.compliance.utils.Util;
import com.Audit4.compliance.utils.Webservice;

import java.io.InputStream;

public class UploadAuditsInfo extends AsyncTask<String, Integer, String> {

    private VendingContentProvider vcp = new VendingContentProvider();
    private static String idValue;

    private SharedPreferences prefs;

    private Context context;
    private AppCompatActivity activity;

    private boolean DEBUG = false;

    private String Date;
    private String UserID;
    private String AuditAssetID;
    private String ServerID;

    public UploadAuditsInfo(Context context, AppCompatActivity activity) {
        this.context = context;
        this.activity = activity;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        prefs = PreferenceManager.getDefaultSharedPreferences(context);

    }


    @Override
    protected String doInBackground(String... params) {

        ServerID = params[9];
        idValue = params[5];
        InputStream is = null;
        String result = null;

        try {
            String[] paramsString = new String[12]; //init
            for (int i = 0; i < 8; i++) {
                paramsString[i] = params[i];
                // Do work.
            }
            paramsString[8] = prefs.getString("CompanyID", "-1");
            paramsString[9] = params[8];
            paramsString[10] = params[9];
            paramsString[11] = "TEAM";
            result = Webservice.uploadCompletedAudit(paramsString, this.DEBUG);


        } catch (Exception e) {
            if (GLOBAL_SETTINGS.DEBUG || this.DEBUG)
                Log.e("log_tag", "Error converting result " + e.toString());
        }

        Date = params[0];
        UserID = params[2];
        AuditAssetID = params[3];
        return result;

    }


    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        if (Util.isString(result) && !result.contains("false: ")) {
            if (GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d("idValue", "id: " + idValue);
            Log.d("ServerID", ServerID);
            if (ServerID.equals("-1")) {
                vcp.addToUploadedAudits(Date, UserID, AuditAssetID, result);
            }
            vcp.UpdateServerID(idValue, result);
//            vcp.getAuditImages(idValue, result);
            vcp.getCompletedAuditAnswers(idValue, result, activity);

       /* if (activity instanceof CompletedAuditListFragment)
            ((CompletedAuditListFragment) activity).updateList();*/

            vcp.getCompletedAudits(context, activity);
        }else{
            Util.showErrorInResponse(activity, result);
        }
    }


}
