package com.Audit4.compliance;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;

import com.Audit4.compliance.dialogfragments.GenericDialogFragment;
import com.Audit4.compliance.utils.RequestHandler;
import com.Audit4.compliance.utils.Util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.util.HashMap;

public class uploadAuditAnswerImages extends AsyncTask<String, Integer, String>{

	private final AppCompatActivity mActivity;

	private InputStream is;
	

	private boolean DEBUG = false;

	public uploadAuditAnswerImages(AppCompatActivity activity) {
		this.mActivity = activity;
	}

	/*@Override
	protected void onPreExecute() {
		((CompletedAuditListFragment) mActivity).showProgressBar("Uploading", "Uploading Data, Please Wait");
	}*/

	@Override
	protected String doInBackground(String... parmas) {

		if(GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d("UPLOAD","UPLOADING IMAGES");

		//0 = id
		//1 = result from server
		//2 = TemplateAuditAnswerID
		//3 = CompanyAuditID
		//4 = Image
		File file = new File(parmas[4]);
		RequestHandler rh = new RequestHandler();
		HashMap<String,String> param = new HashMap<String,String>();
		param.put("CompletedAuditID",parmas[0]);
		param.put("Name",file.getName());
		if (parmas[2] != null)
		{
			param.put("TemplateAuditAnswerID", parmas[2]);
			param.put("CompanyAuditAnswerID", "");
		}
		else
		{
			param.put("TemplateAuditAnswerID", "");
			param.put("CompanyAuditAnswerID", parmas[3] == null? "" : parmas [3]);
		}

		Bitmap bitmap = BitmapFactory. decodeFile(file. getAbsolutePath());
		String imageStr = getStringImage(bitmap);
		param.put("Image", imageStr);
		String response = rh.sendPostRequest(WebsiteUrl.webURl +"includes/APIS/uploadCompletedAuditAnswerImage.php", param);


		return response;
	}

	@Override
	protected void onPostExecute(String result) {
		if (mActivity instanceof CompletedAuditListFragment) {
			((CompletedAuditListFragment) mActivity).hideProgressBar();
            if (result.equals("error")) {
//			if (Util.isString(result) && result.contains("false: ")) {
				GenericDialogFragment loginErrorFragment = new GenericDialogFragment();
				loginErrorFragment.setGenericDialogFragment("Audit Upload", "Error Uploading data", "", "Cancel");
				if (!mActivity.isFinishing())
					loginErrorFragment.show(mActivity.getSupportFragmentManager(), "Audit");
			} else {
				if (mActivity instanceof CompletedAuditListFragment)
					((CompletedAuditListFragment) mActivity).updateList();

			}
		} else {
			((Home) mActivity).hideProgressBar();
			if (result.equals("error")) {
				GenericDialogFragment loginErrorFragment = new GenericDialogFragment();
				loginErrorFragment.setGenericDialogFragment("Audit", "Error Uploading data", "", "Cancel");
				loginErrorFragment.show(mActivity.getSupportFragmentManager(), "Audit");
			}
		}
	}	

	public String getStringImage(Bitmap bmp){
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
		byte[] imageBytes = baos.toByteArray();
		String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
		return encodedImage;
	}
}
