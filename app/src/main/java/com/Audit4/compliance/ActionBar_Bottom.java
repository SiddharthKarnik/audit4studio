package com.Audit4.compliance;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

import com.Audit4.compliance.audit.AuditFragment;


public class ActionBar_Bottom extends Fragment {
	
	public interface buttonListener
	{
		public void buttonClicked(String which);
	}
	
	private boolean DEBUG = false;
	
	buttonListener mListener;
	
	Button buttonA;
	Button buttonB;
	Button buttonC;
	Button buttonD;
	
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {
        // Inflate the layout for this fragment
      View view = inflater.inflate(R.layout.actionbar_bottom, container, false);
        
		buttonA = (Button) view.findViewById(R.id.actionButtonA);
		buttonB = (Button) view.findViewById(R.id.actionButtonB);
		buttonC = (Button) view.findViewById(R.id.actionButtonC);
		buttonD = (Button) view.findViewById(R.id.actionButtonD);

		return view;
    }
    
    @Override
    public void onAttach(Activity activity)
    {
    	super.onAttach(activity);
    	 // Verify that the host activity implements the callback interface
	     try {
	     	// Instantiate the NoticeDialogListener so we can send events to the host
	    	mListener = (buttonListener) activity;
	     } catch (ClassCastException e) {
	    	 // The activity doesn't implement the interface, throw exception
	      	 throw new ClassCastException(activity.toString() + " must implement NoticeDialogListener");
	        }
    }

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		buttonA.setOnClickListener(new OnClickListener() 
		{
		public void onClick(View v) 
			{mListener.buttonClicked("A");}
		});
		
		buttonB.setOnClickListener(new OnClickListener() 
		{
		public void onClick(View v) 
			{mListener.buttonClicked("B");}
		});
		
		buttonC.setOnClickListener(new OnClickListener() 
		{
		public void onClick(View v) 
			{mListener.buttonClicked("C");}
		});
		
		buttonD.setOnClickListener(new OnClickListener() 
		{
		public void onClick(View v) 
			{mListener.buttonClicked("D");}
		});
				
	}

	@Override
	public void onResume() {
		super.onResume();
		
		if (getActivity().getClass().getSimpleName().equals("Summary"))
		{
			buttonA.setText("Cancel");
			buttonB.setVisibility(View.INVISIBLE);
			buttonC.setVisibility(View.INVISIBLE);
			buttonD.setText("Save");
		}
		
		if (getActivity().getClass().getSimpleName().equals("Audit"))
		{
			if (AuditFragment.editMode == false)
			{
			buttonA.setText("Previous");
			buttonB.setText("Notes");
			buttonC.setText("Camera");
			buttonD.setText("Next");
			}
			else
			{
			buttonA.setText("Cancel");
			buttonB.setText("Notes");
			buttonC.setText("Camera");
			buttonD.setText("Save");
			}	
		}
	}
	
	

    
}