package com.Audit4.compliance;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class GLOBAL_SETTINGS {
	public static boolean DEBUG = false;
	
	public static boolean networkIsOnline(Activity activity) {
	    boolean status=false;
	    try{
	        ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
	        NetworkInfo netInfo = cm.getNetworkInfo(0);
	        if (netInfo != null && netInfo.getState()==NetworkInfo.State.CONNECTED) {
	            status= true;
	        }
	        else {
	        	netInfo = cm.getNetworkInfo(1);
	            if(netInfo!=null && netInfo.getState()==NetworkInfo.State.CONNECTED)
	                status= true;
	        }
	    }
	    catch(Exception e){
	        e.printStackTrace();  
	        return false;
	    }
	    return status;
	}
	
	/* MESSAGE TITLES */
	public static String homeTitle = "Audit Category:";
	public static String auditTitle = "Audit:";
	public static String summaryTitle = "Audit Summary:";
	public static String previousAuditsTitle = "Previous Audits:";
	public static String previousAuditSummaryTitle = "Previous Audit Summary:";
	public static String uploadAuditTitle = "Audit Upload:";
	/* MESSAGE TITLES */
	
	/* MESSAGES */
		/* PROGRESS MESSAGES */
		public static String loadingAuditQuestions = "Loading Audit Questions...";
		public static String savingQuestion = "Saving Question...";
		public static String loadingPreviousAudits = "Loading Previous Audits...";
		
		public static String uploadingAuditInfo = "Upoading Audit Info...";
		public static String uploadingAuditQuestions = "Upoading Audit Questions...";
		public static String uploadingAuditImages = "Upoading Audit Images...";
		
		public static String loadingPreviousAuditSummary = "Loading Previous Audit Summary...";
		/* PROGRESS MESSAGES */
	
	public static String cancelAudit = "Are you sure you wish cancel the audit? All progress will be lost.";
	public static String homescreenReturnError = "Do you wish to move away from this screen? If you haven't uploaded your audit, all changes will be lost.";
	public static String unansweredQuestionsError = "You have unanswered questions in this audit, you can edit a question by tapping it. Alternatively you can upload without answering all questions.";
	public static String selectAnswerError = "All answers must be answered before submiting the audit, do you wish to continue without answering!";
	public static String genericError = "An error has occured, please try again!";
	public static String networkError = "An error occured, please check your network connection!";
	public static String noDataError = "No data found for selected audit!";
	public static String parseError = "An error ocurred when retreiving data from the server, please try again!";
	public static String uploadComplete = "Upload complete! You will now be returned to the home screen.";
	/* MESSAGES */
	
	/*IMAGE CAPTURE SIZE*/
	public static int PHOTO_WIDTH = 800;
	public static int PHOTO_HEIGHT = 480;
	
}
