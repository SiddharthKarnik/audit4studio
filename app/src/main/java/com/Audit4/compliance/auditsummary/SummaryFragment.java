package com.Audit4.compliance.auditsummary;

import android.app.ListFragment;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import com.Audit4.compliance.audit.AuditFragment;
import com.Audit4.compliance.contentprovider.VendingContentProvider;
import com.Audit4.compliance.R;

public class SummaryFragment extends ListFragment{
	
	private VendingContentProvider vcp;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
    }
	
	@Override
	public void onStart()
	{
		super.onStart();
		setListAdapter(new SummaryAdapter(getActivity(), R.layout.summary_row, Summary.completedQuestions));         

	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		//Log.d("CLICK","CLICK: " + position);
		AuditFragment.questionNumber = position;
		AuditFragment.editMode = true;
		getActivity().finish();
	}
	
}
