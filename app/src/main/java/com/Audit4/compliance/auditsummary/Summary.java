package com.Audit4.compliance.auditsummary;

import java.util.ArrayList;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.Audit4.compliance.CompletedQuestion;
import com.Audit4.compliance.GLOBAL_SETTINGS;
import com.Audit4.compliance.Home;
import com.Audit4.compliance.ActionBar_Bottom.buttonListener;
import com.Audit4.compliance.audit.AuditFragment;
import com.Audit4.compliance.dialogfragments.GenericDialogFragment;
import com.Audit4.compliance.R;

public class Summary extends AppCompatActivity implements buttonListener,  GenericDialogFragment.NoticeDialogListener {
	private static String TAG = "Summary";
	static String auditName, client, clientSite;
	static int auditID, TemplateID;
	static String routeID, machineID;
	public static String Template;
	public static ArrayList<CompletedQuestion> completedQuestions; //list to store all completed questions
	static String dialogType;
	
	SummaryDetails save = new SummaryDetails();
	
	private boolean DEBUG = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_summary);
		if (savedInstanceState == null) {
			auditName = getIntent().getStringExtra("auditName");
			auditID = getIntent().getIntExtra("AuditID",-1);
			TemplateID = getIntent().getIntExtra("TemplateID",-1);
			Template = getIntent().getStringExtra("Template");
			routeID = getIntent().getStringExtra("routeID");
			machineID = getIntent().getStringExtra("MachineID");
			if(GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d(TAG,"Machine Id: " + machineID);
			completedQuestions = (ArrayList<CompletedQuestion>) getIntent().getSerializableExtra("completedQuestions");
			client = getIntent().getStringExtra("Client");
			clientSite = getIntent().getStringExtra("ClientSite");			
			
			getSupportActionBar().setTitle(auditName + " Summary"); //set title
			
			for (CompletedQuestion cq : completedQuestions)
			{
				if(GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d("ANSWERGRADE",cq.getAnswerGrade());
			}
			
			if(GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d("ListSet","arrayListSet: " + completedQuestions.size());
		}
	}
	
	@Override
	public void onBackPressed() {
		dialogType = "Cancel Audit";
		showAlertDialog("Cancel Audit");
	}
	
	@Override
	public void buttonClicked(String which) {
		switch (which) {
			case "D": //upload button
				
				//SummaryDetails save = new SummaryDetails();
				if(!save.inProgress) save.saveAudit();
				
				dialogType = "Audit Saved";
				showAlertDialog("Audit Saved");
				
	 			break;
			case "A": //cancel button
				dialogType = "Cancel Audit";
				showAlertDialog("Cancel Audit");
				if(GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d(TAG,"Cancel button pressed.");
				break;
		}
	}
		
	public void showAlertDialog(String dialogType) {
		String title = null;
		String message = null;
		String button1 = null;
		String button2 = null;
		
		switch(dialogType) {
			case "Cancel Audit":
				title = "Audit Summary";
				message = "Are you sure you wish to end this audit? All current progress will be lost!";
				button1 = "End Audit";
				button2 = "Cancel";
				break;
				
			case "Audit Saved":
				title ="Audit Saved";
				message = "Audit Sucessfully Saved";
				button1 = "Continue";
				button2 = "";
				break;
		}
		GenericDialogFragment newFragment = new GenericDialogFragment();
		newFragment.setGenericDialogFragment(title, message,button1,button2);
	    newFragment.setCancelable(false);
	    newFragment.show(getSupportFragmentManager(), "alert");
	}
	
	@Override
	public void onDialogPositiveClick(DialogFragment dialog) {
		if(dialogType.equals("Cancel Audit"))
			{
			AuditFragment.auditActivity.finish();
			finish();
			}
		else if(dialogType.equals("Upload Error")) dialog.dismiss();
		else if(dialogType.equals("Audit Saved")) 
		{
			Intent intent = new Intent(this,Home.class);
			ActivityOptions opts=ActivityOptions.makeCustomAnimation(getBaseContext(), 
					R.anim.fade_in, R.anim.fade_out);
	    	intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP| Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TASK);
			startActivity(intent,opts.toBundle());
		}
			
	}

	@Override
	public void onDialogNegativeClick(DialogFragment dialog) {	

	}	
	
	
}


