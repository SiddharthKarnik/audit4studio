package com.Audit4.compliance.auditsummary;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.Audit4.compliance.CompletedQuestion;
import com.Audit4.compliance.GLOBAL_SETTINGS;
import com.Audit4.compliance.contentprovider.VendingContentProvider;
import com.Audit4.compliance.R;
import com.Audit4.compliance.utils.Util;

public class SummaryDetails extends Fragment {
	
	private VendingContentProvider vcp = new VendingContentProvider();

	private TextView RouteID;
	private TextView MachineID;
	private TextView Date;
	private TextView Score;
	private Context context;
	private SharedPreferences prefs;
	private static String userID;
	
	public boolean inProgress = false;  //used to register whether an audit is complete and being saved - see Summary.java 
	
	private boolean DEBUG = false;
		
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_summary_details, container, false);
        
        RouteID = (TextView) view.findViewById(R.id.Date);
        MachineID = (TextView) view.findViewById(R.id.summaryMachineID);
        Date = (TextView) view.findViewById(R.id.summaryDate);
        Score = (TextView) view.findViewById(R.id.summaryScore);
        
        context = this.getActivity().getApplicationContext();
		prefs = PreferenceManager.getDefaultSharedPreferences(context);
		
		userID = prefs.getString("UserID", "-1");
		if(GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d("USERID",userID);
        
        return view;
    }
    
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		String[] AuditTypeText = getAuditTypeText();

		RouteID.setText(AuditTypeText[0] + Summary.routeID);
		MachineID.setText(AuditTypeText[1] + Summary.machineID);
		
		String _date = getDate();
		Date.setText("Date: " + _date);
		
		int _score = getScore();
		Score.setText("Score: " + _score + "%");
		
		int companyBenchmark = vcp.getCompanyBenchmark(Summary.Template);
		
		
		if(_score < companyBenchmark) Score.setTextColor(Color.RED);
		else Score.setTextColor(Color.GREEN);
			
	}
	
	String[] getAuditTypeText()
	{
		int AuditType = Summary.TemplateID;
		
		String[] AuditText = new String[2];
		String[] headers = vcp.getHeadersForAssetType(AuditType);
		if(Util.isString(headers[3])){
			AuditText[0] = headers[3]+": ";
		}else {
			AuditText[0] = "N/A: ";
			RouteID.setVisibility(View.GONE);
		}
		AuditText[1] = headers[0]+": ";
		return AuditText;
		
	}
	
	public String getDate() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		String dateString = dateFormat.format(date).toString();
		return dateString;
	}
	
	public int getScore() {
		int total = 0;
		int totalAvailable = 0;
		float percentage;
		for (CompletedQuestion cq : Summary.completedQuestions) {
			int[] scores = new int[2];
			if(GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d("Answer", "Answer Grade: " + cq.getAnswerGrade() + "QuestionRank: " + cq.getQuestionRank());
			scores = vcp.getScore(cq.getQuestionRank(),cq.getAnswerGrade());
			
			if(GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d("Scores",scores[0] + " " + scores[1]);
			
			
			total = total + scores[1];
			totalAvailable = totalAvailable + scores[0];
		}
		
		if(GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d("Total","" +total);
		if(GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d("totalAvailable","" + totalAvailable);
		
		if(totalAvailable == 0) percentage = 0;
		else percentage = ((total*100)/totalAvailable);
	
		return Math.round(percentage);
	}
	
	public String getCompanyAuditID()
	{
		return vcp.getCompanyAuditID(Summary.Template);
	}
	
	public void saveAudit() {
 
		if(GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d("USERID",userID);

		inProgress = true;
		
		vcp.insertCompleteAudit(getScore(), Summary.machineID, Summary.routeID, getDate(),userID,Summary.client,Summary.clientSite,getCompanyAuditID());
		int CompletedAuditID = vcp.getLastAudit();
		
		int answerID = -1;
		
		for (CompletedQuestion cq : Summary.completedQuestions) {
		
		if (cq.getTemplateAuditQuestionID() != -1)
			{
			answerID = vcp.getTemplateAnswerID(cq.getTemplateAuditQuestionID(), cq.getAnswerGrade());
			vcp.insertCompletedAuditAnswer(CompletedAuditID, answerID, -1, cq.getAdditionalNotes(),cq.getQuestionNumber()+1); //TODO questionNumber
			vcp.insertCompletedAuditAnswerImage(CompletedAuditID, answerID, -1, cq.getImagePaths());
			
			}
		else
			{
			answerID = vcp.getCompanyAnswerID(cq.getCompanyAuditID(), cq.getAnswerGrade());
			vcp.insertCompletedAuditAnswer(CompletedAuditID, -1, answerID, cq.getAdditionalNotes(),cq.getQuestionNumber()+1); //TODO questionNumber
			vcp.insertCompletedAuditAnswerImage(CompletedAuditID, -1, answerID, cq.getImagePaths());

			}
		}
		
	}
}
