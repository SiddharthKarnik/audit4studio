package com.Audit4.compliance.auditsummary;

import java.util.List;
import java.util.Set;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.Audit4.compliance.CompletedQuestion;
import com.Audit4.compliance.GLOBAL_SETTINGS;
import com.Audit4.compliance.audit.Audit;
import com.Audit4.compliance.contentprovider.VendingContentProvider;
import com.Audit4.compliance.R;

public class SummaryAdapter extends ArrayAdapter<CompletedQuestion>{
	
	private List<CompletedQuestion> _list;
	private Context _context;
	private int _layout;
	private VendingContentProvider vcp;
	
	private boolean DEBUG = false;

	public SummaryAdapter(Context context, int resource, List<CompletedQuestion> objects) {
		super(context, resource, objects);
		_context = context;
		_layout = resource;
		_list = objects;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if(GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d("Adapter","Adapter");

		View row = convertView;
		
		LayoutInflater inflater = (LayoutInflater)_context.getSystemService("layout_inflater") ;
		row = inflater.inflate(_layout, null);
		
		CompletedQuestion _cq = _list.get(position);
				
		//Question Number
		TextView QuestionNumber = (TextView) row.findViewById(R.id.summaryNumber);
		QuestionNumber.setText(position+1 + ": ");
		
		
		//Question
		TextView Question = (TextView) row.findViewById(R.id.summaryQuestion);
		Question.setText(Audit.questionList.get(position).getTitle());
		
		//Set Colour
		LinearLayout layout = (LinearLayout) row.findViewById(R.id.summaryRow);
		if(GLOBAL_SETTINGS.DEBUG || this.DEBUG) Log.d("AnswerGrade",_cq.getAnswerGrade());
		if (_cq.getAnswerGrade().equals("L")) {
			Question.setBackgroundResource(R.color.red);
			QuestionNumber.setBackgroundResource(R.color.red);
		}
		else if(_cq.getAnswerGrade().equals("M")) {
			Question.setBackgroundResource(R.color.amber);
			QuestionNumber.setBackgroundResource(R.color.amber);
		}
		else if(_cq.getAnswerGrade().equals("H")) {
			Question.setBackgroundResource(R.color.green);
			QuestionNumber.setBackgroundResource(R.color.green);
		}
		else if(_cq.getAnswerGrade().equals("NONE")) {
			Question.setBackgroundResource(R.color.gray);
			QuestionNumber.setBackgroundResource(R.color.gray);
		} 
       	return row;
	}
}