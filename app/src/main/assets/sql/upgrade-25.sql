CREATE TABLE IF NOT EXISTS AuditAssetHeaders (
  `AuditAssetHeadersID` int(10) NOT NULL AUTO_INCREMENT,
  `AuditAssetTypeID` int(10) NOT NULL,
  `Header1` varchar(100) NOT NULL,
  `Header2` varchar(100) NOT NULL,
  `Header3` varchar(100) NOT NULL,
  `Header4` varchar(100) DEFAULT NULL,
  `Header5` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`AuditAssetHeadersID`)
);


