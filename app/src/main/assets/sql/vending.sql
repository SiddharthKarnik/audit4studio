CREATE TABLE QuestionType (
_id INTEGER PRIMARY KEY,
QuestionType TEXT,
NumberOfAnswers INTEGER
);

CREATE TABLE QuestionRank (
_id INTEGER PRIMARY KEY,
QuestionRank TEXT,
RedScore INTEGER,
AmberScore INTEGER,
GreenScore INTEGER
);

CREATE TABLE TemplateAudit (
_id INTEGER PRIMARY KEY,
TemplateAuditName TEXT,
AuditType INTEGER
);

CREATE TABLE TemplateAuditQuestionSection (
_id INTEGER PRIMARY KEY,
TemplateAuditSectionName TEXT,
TemplateAuditID INTEGER REFERENCES TemplateAudit(_id)
);

CREATE TABLE TemplateAuditQuestion (
_id INTEGER PRIMARY KEY,
TemplateAuditQuestionTitle TEXT,
TemplateAuditQuestion TEXT,
TemplateAuditQuestionSectionID INTEGER REFERENCES TemplateAuditQuestionSection(_id),
QuestionTypeID INTEGER REFERENCES QuestionType(_id),
QuestionRankID INTEGER REFERENCES QuestionRank(_id),
TemplateAuditID INTEGER REFERENCES TemplateAudit(_id)
);

CREATE TABLE TemplateAuditAnswer (
_id INTEGER PRIMARY KEY,
TemplateAuditAnswerGrade TEXT,
TemplateAuditAnswer TEXT,
TemplateAuditQuestionID INTEGER REFERENCES TemplateAuditQuestion(_id)
);

CREATE TABLE CompanyAudit (
_id INTEGER PRIMARY KEY,
CompanyAuditName TEXT,
CompanyAuditBenchmark INTEGER,
TemplateAuditID INTEGER REFERENCES TemplateAudit(_id)
);

CREATE TABLE CompanyAuditQuestion (
_id INTEGER PRIMARY KEY,
CompanyAuditQuestionTitle TEXT,
CompanyAuditQuestion TEXT, 
QuestionTypeID INTEGER REFERENCES QuestionType(_id),
QuestionRankID INTEGER REFERENCES QuestionRank(_id),
CompanyAuditID INTEGER REFERENCES CompanyAudit(_id),
TemplateAuditQuestionSectionID INTEGER REFERENCES TemplateAuditQuestionSection(_id)
);

CREATE TABLE CompanyAuditAnswer (
_id INTEGER PRIMARY KEY,
CompanyAuditAnswerGrade TEXT,
CompanyAuditAnswer TEXT,
CompanyAuditQuestionID INTEGER REFERENCES CompanyAuditQuestion(_id)
);

CREATE TABLE CompletedAudit (
_id INTEGER PRIMARY KEY,
AuditDate TEXT,
TotalScore INTEGER,
UserID INTEGER REFERENCES User(_id),
AuditAssetID INTEGER,
RouteID INTEGER,
Client TEXT,
ClientSite TEXT,
CompanyAuditID INTEGER,
ServerID INTEGER DEFAULT -1
);

CREATE TABLE CompletedAuditAnswer (
_id INTEGER PRIMARY KEY,
CompletedAuditID INTEGER REFERENCES CompletedAudit(_id),
TemplateAuditQuestionID INTEGER REFERENCES TemplateAuditQuestion(_id),
CompanyAuditQuestionID INTEGER REFERENCES CompanyAuditQuestion(_id),
TemplateAuditAnswerID INTEGER REFERENCES TemplateAuditAnswer(_id),
CompanyAuditAnswerID INTEGER REFERENCES CompanyAuditAnswer(_id),
AdditionalNotes VARCHAR(150),
QuestionNumber INTEGER
);

CREATE TABLE CompletedAuditAnswerImage (
_id INTEGER PRIMARY KEY,
CompletedAuditID INTEGER REFERENCES CompletedAudit(_id),
TemplateAuditQuestionID INTEGER REFERENCES TemplateAuditQuestion(_id),
CompanyAuditQuestionID INTEGER REFERENCES CompanyAuditQuestion(_id),
TemplateAuditAnswerID INTEGER REFERENCES TemplateAuditAnswer(_id),
CompanyAuditAnswerID INTEGER REFERENCES CompanyAuditAnswer(_id),
ImagePath INTEGER
);

CREATE TABLE AuditsToUpload (
_id INTEGER PRIMARY KEY,
CompletedAuditID INTEGER REFERENCES CompletedAudit(_id)
);

CREATE TABLE UploadedAudits (
_id INTEGER PRIMARY KEY,
AuditDate TEXT,
UserID INTEGER REFERENCES User(_id),
AuditAssetID INTEGER,
ServerID INTEGER
);

CREATE TABLE AuditAssetHeaders (
  _id INTEGER PRIMARY KEY,
  Header1 TEXT NOT NULL,
  Header2 TEXT NOT NULL,
  Header3 TEXT NOT NULL,
  Header4 TEXT DEFAULT NULL,
  Header5 TEXT DEFAULT NULL
);


